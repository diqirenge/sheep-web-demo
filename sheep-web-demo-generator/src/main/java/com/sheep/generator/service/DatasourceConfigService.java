package com.sheep.generator.service;

import com.sheep.generator.entity.DatasourceConfig;
import com.sheep.generator.gen.DbType;
import com.sheep.generator.mapper.DatasourceConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 第七人格
 */
@Service
public class DatasourceConfigService {

    @Autowired
    private DatasourceConfigMapper datasourceConfigMapper;

    public DatasourceConfig getById(int id) {
        return datasourceConfigMapper.getById(id);
    }

    public List<DatasourceConfig> listAll() {
        return datasourceConfigMapper.listAll();
    }

    public void insert(DatasourceConfig templateConfig) {
        templateConfig.setIsDeleted(0);
        DbType dbType = DbType.of(templateConfig.getDbType());
        if (dbType != null) {
            templateConfig.setDriverClass(dbType.getDriverClass());
        }
        datasourceConfigMapper.insert(templateConfig);
    }

    public void update(DatasourceConfig templateConfig) {
        datasourceConfigMapper.update(templateConfig);
    }

    public void delete(DatasourceConfig templateConfig) {
        datasourceConfigMapper.delete(templateConfig);
    }
}
