package com.sheep.generator.controller;

import com.sheep.generator.common.Action;
import com.sheep.generator.common.GeneratorParam;
import com.sheep.generator.common.Result;
import com.sheep.generator.entity.DatasourceConfig;
import com.sheep.generator.gen.GeneratorConfig;
import com.sheep.generator.service.DatasourceConfigService;
import com.sheep.generator.service.GeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 第七人格
 */
@RestController
@RequestMapping("generate")
public class GeneratorController {

    @Autowired
    private DatasourceConfigService datasourceConfigService;

    @Autowired
    private GeneratorService generatorService;

    /**
     * 生成代码
     *
     * @param generatorParam 生成参数
     * @return 返回代码内容
     */
    @RequestMapping("/code")
    public Result code(@RequestBody GeneratorParam generatorParam) {
        int datasourceConfigId = generatorParam.getDatasourceConfigId();
        DatasourceConfig datasourceConfig = datasourceConfigService.getById(datasourceConfigId);
        GeneratorConfig generatorConfig = GeneratorConfig.build(datasourceConfig);
        return Action.ok(generatorService.generate(generatorParam, generatorConfig));
    }

}
