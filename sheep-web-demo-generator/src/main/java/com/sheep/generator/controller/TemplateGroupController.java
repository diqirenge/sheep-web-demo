package com.sheep.generator.controller;

import com.sheep.generator.common.Action;
import com.sheep.generator.common.Result;
import com.sheep.generator.entity.TemplateGroup;
import com.sheep.generator.service.TemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : 第七人格
 */
@RestController
@RequestMapping("group")
public class TemplateGroupController {

    @Autowired
    private TemplateGroupService templateGroupService;

    /**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    @RequestMapping("list")
    public Result listAll() {
        List<TemplateGroup> templateGroups = templateGroupService.listAll();
        return Action.ok(templateGroups);
    }


    /**
     * 根据主键查询
     *
     * @param id 主键
     * @return 返回记录，没有返回null
     */
    @RequestMapping("get/{id}")
    public Result get(@PathVariable("id") int id) {
        TemplateGroup group = templateGroupService.getById(id);
        return Action.ok(group);
    }

    /**
     * 新增，忽略null字段
     *
     * @param templateGroup 新增的记录
     * @return 返回影响行数
     */
    @RequestMapping("add")
    public Result insert(@RequestBody TemplateGroup templateGroup) {
        templateGroupService.insertIgnoreNull(templateGroup);
        return Action.ok(templateGroup);
    }

    /**
     * 修改，忽略null字段
     *
     * @param templateGroup 修改的记录
     * @return 返回影响行数
     */
    @RequestMapping("update")
    public Result update(@RequestBody TemplateGroup templateGroup) {
        templateGroupService.updateIgnoreNull(templateGroup);
        return Action.ok();
    }

    /**
     * 删除记录
     *
     * @param templateGroup 待删除的记录
     * @return 返回影响行数
     */
    @RequestMapping("del")
    public Result delete(@RequestBody TemplateGroup templateGroup) {
        templateGroupService.deleteGroup(templateGroup);
        return Action.ok();
    }

}