package com.sheep.generator.gen.mysql;

import com.sheep.generator.gen.GeneratorConfig;
import com.sheep.generator.gen.SQLService;
import com.sheep.generator.gen.TableSelector;

public class MySqlService implements SQLService {

	@Override
	public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
		return new MySqlTableSelector(new MySqlColumnSelector(generatorConfig), generatorConfig);
	}

}
