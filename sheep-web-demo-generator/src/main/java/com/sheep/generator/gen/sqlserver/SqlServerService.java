package com.sheep.generator.gen.sqlserver;

import com.sheep.generator.gen.GeneratorConfig;
import com.sheep.generator.gen.SQLService;
import com.sheep.generator.gen.TableSelector;

public class SqlServerService implements SQLService {

	@Override
	public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
		return new SqlServerTableSelector(new SqlServerColumnSelector(generatorConfig), generatorConfig);
	}

}
