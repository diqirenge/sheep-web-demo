package com.sheep.generator.gen;


public interface SQLService {

	TableSelector getTableSelector(GeneratorConfig generatorConfig);

}
