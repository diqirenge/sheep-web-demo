package com.sheep.generator.gen.oracle;

import com.sheep.generator.gen.GeneratorConfig;
import com.sheep.generator.gen.SQLService;
import com.sheep.generator.gen.TableSelector;

public class OracleService implements SQLService {

	@Override
	public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
		return new OracleTableSelector(new OracleColumnSelector(generatorConfig), generatorConfig);
	}

}
