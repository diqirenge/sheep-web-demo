package com.sheep.generator.gen;

import com.sheep.generator.gen.converter.ColumnTypeConverter;
import com.sheep.generator.gen.converter.CsharpColumnTypeConverter;
import com.sheep.generator.util.FieldUtil;

/**
 * 提供C# Velocity变量
 * @author 第七人格
 */
public class CsharpColumnDefinition extends ColumnDefinition {

    private static final ColumnTypeConverter COLUMN_TYPE_CONVERTER = new CsharpColumnTypeConverter();

    public String getField() {
        return FieldUtil.underlineFilter(getColumnName());
    }

    public String getProperty() {
        return FieldUtil.upperFirstLetter(getField());
    }

    @Override
    public ColumnTypeConverter getColumnTypeConverter() {
        return COLUMN_TYPE_CONVERTER;
    }
}
