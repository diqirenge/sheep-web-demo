package com.sheep.generator.gen.postgresql;

import com.sheep.generator.gen.GeneratorConfig;
import com.sheep.generator.gen.SQLService;
import com.sheep.generator.gen.TableSelector;

/**
 * @author 第七人格
 */
public class PostgreSqlService implements SQLService {
    @Override
    public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
        return new PostgreSqlTableSelector(new PostgreSqlColumnSelector(generatorConfig), generatorConfig);
    }

}
