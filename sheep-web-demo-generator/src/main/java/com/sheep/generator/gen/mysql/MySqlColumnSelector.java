package com.sheep.generator.gen.mysql;

import com.sheep.generator.gen.ColumnDefinition;
import com.sheep.generator.gen.ColumnSelector;
import com.sheep.generator.gen.GeneratorConfig;
import com.sheep.generator.gen.TypeEnum;
import com.sheep.generator.gen.TypeFormatter;

import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * mysql表信息查询
 */
public class MySqlColumnSelector extends ColumnSelector {

    private static final TypeFormatter TYPE_FORMATTER = new MySqlTypeFormatter();

    private static final String SHOW_SQL = "SHOW FULL COLUMNS FROM `%s`";

    public MySqlColumnSelector(GeneratorConfig generatorConfig) {
        super(generatorConfig);
    }

    /**
     * SHOW FULL COLUMNS FROM 表名
     */
    @Override
    protected String getColumnInfoSQL(String tableName) {
        return String.format(SHOW_SQL, tableName);
    }

    /*
     * {FIELD=username, EXTRA=, COMMENT=用户名, COLLATION=utf8_general_ci, PRIVILEGES=select,insert,update,references, KEY=PRI, NULL=NO, DEFAULT=null, TYPE=varchar(20)}
     */
    @Override
    protected ColumnDefinition buildColumnDefinition(Map<String, Object> rowMap) {
        Set<String> columnSet = rowMap.keySet();

        for (String columnInfo : columnSet) {
            rowMap.put(columnInfo.toUpperCase(), rowMap.get(columnInfo));
        }

        ColumnDefinition columnDefinition = new ColumnDefinition();

        columnDefinition.setColumnName((String) rowMap.get("FIELD"));

        boolean isIdentity = "auto_increment".equalsIgnoreCase((String) rowMap.get("EXTRA"));
        columnDefinition.setIsIdentity(isIdentity);

        boolean isPk = "PRI".equalsIgnoreCase((String) rowMap.get("KEY"));
        columnDefinition.setIsPk(isPk);

        String type = (String) rowMap.get("TYPE");
        columnDefinition.setType(TYPE_FORMATTER.format(type));

        // 获取括号中的数据
        Pattern pattern = Pattern.compile("(?<=\\()[^\\)]+");
        Matcher matcher = pattern.matcher(type);
        String columnDefinitionType = columnDefinition.getType();
        while (matcher.find()) {
            String group = matcher.group();
            if (TypeEnum.VARCHAR.getType().equals(columnDefinitionType)){
                columnDefinition.setStrLength(group);
            }else if (TypeEnum.INT.getType().equals(columnDefinitionType)){
                columnDefinition.setStrLength(group);
            }else if (TypeEnum.DECIMAL.getType().equals(columnDefinitionType)){
                String[] split = group.split(",");
                Integer decimal = Integer.valueOf(split[1]);
                int i = Integer.valueOf(split[0]) - decimal;
                StringBuilder a = new StringBuilder("9");
                for (int j=1; j<i; j++){
                    a.append(9);
                }
                StringBuilder a2 = new StringBuilder();
                if (decimal != 0){
                    a2 = a2.append(9);
                    for (int j=1; j<decimal; j++){
                        a2.append(9);
                    }
                    a2 = new StringBuilder(".").append(a2);
                }

                columnDefinition.setStrLength(a.toString()+a2.toString());
            }

        }

        columnDefinition.setComment((String) rowMap.get("COMMENT"));

        return columnDefinition;
    }

}
