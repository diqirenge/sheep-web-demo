package com.sheep.generator.config;

import com.sheep.generator.gen.DataSourceManager;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

/**
 * 监听器
 *
 * @author 第七人格
 */
@WebListener
public class RequestListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		DataSourceManager.closeConnection();
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
	}

}

