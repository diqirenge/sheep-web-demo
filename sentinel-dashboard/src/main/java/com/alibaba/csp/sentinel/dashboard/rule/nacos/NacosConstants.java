package com.alibaba.csp.sentinel.dashboard.rule.nacos;


/**
 * Nacos常量
 *
 * @author lizy
 * @date 2021/12/07
 */
public class NacosConstants {
    public static final String DATA_ID_POSTFIX = "-sentinel-flow";
    public static final String GROUP_ID = "DEFAULT_GROUP";
}
