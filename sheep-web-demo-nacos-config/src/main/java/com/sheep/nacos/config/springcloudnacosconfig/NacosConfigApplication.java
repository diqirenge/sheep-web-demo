package com.sheep.nacos.config.springcloudnacosconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class NacosConfigApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context=
				SpringApplication.run(NacosConfigApplication.class, args);
		String info=context.getEnvironment().getProperty("info");
		System.out.println(info);
	}
}
