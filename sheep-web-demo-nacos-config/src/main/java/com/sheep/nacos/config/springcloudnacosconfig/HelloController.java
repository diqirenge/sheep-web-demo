package com.sheep.nacos.config.springcloudnacosconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 你好控制器
 *
 * @author 第七人格
 * @date 2021/12/02
 */
@RestController
// 动态刷新注解
@RefreshScope
public class HelloController {


    @Value(value = "${info:nb}")
    private String info;

    @GetMapping("/config")
    public String get(){
        System.out.println(info);
        return info;
    }
}
