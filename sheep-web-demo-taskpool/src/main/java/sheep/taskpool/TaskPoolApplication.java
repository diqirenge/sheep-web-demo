package sheep.taskpool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskPoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskPoolApplication.class, args);
    }

}
