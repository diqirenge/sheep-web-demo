package com.sheep.power.dto;

import lombok.Data;

/**
 * @author lizy4
 * @date 2021/8/7 
 */
@Data
public class TestDto {
    private String name;
    private String value;
}
