package com.sheep.power.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sheep.power.annotation.PowerRequestBody;
import com.sheep.power.dto.TestDto;

/**
 * 测试controller
 *
 * @author lizy4
 * @date 2021/8/7
 */
@RestController
@RequestMapping("/power")
public class PowerController {

    @PostMapping("/powerRequestBody")
    public Object powerRequestBody(@PowerRequestBody String dto){
        return "powerRequestBody执行成功："+dto;
    }

    @PostMapping("/requestBody")
    public Object requestBody(@RequestBody TestDto dto){
        return "requestBody执行成功："+dto;
    }
}
