# sheep-web-demo-pwoerRequestBody

> 本 demo 主要演示了如何增强@RequestBody注解

解决的问题：
* 1、单个字符串等包装类型都要写一个对象才可以用@RequestBody接收；
* 2、多个对象需要封装到一个对象里才可以用@RequestBody接收。