package com.sheep.start.swagger.autoconfigure;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Objects;


/**
 * swagger属性条件类
 *
 * @author 第七人格
 * @date 2021/09/15
 */
public class OnSwaggerCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String basePackage = context.getEnvironment().getProperty("swagger.basePackage");
        if (Objects.isNull(basePackage)) {
            throw new RuntimeException("basePackage不允许为空！");
        } else {
            return true;
        }
    }
}