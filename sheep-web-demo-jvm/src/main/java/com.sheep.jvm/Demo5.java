package com.sheep.jvm;

/**
 * 模拟老年代FullGC的触发
 * -XX:NewSize=10485760 -XX:MaxNewSize=10485760 -XX:InitialHeapSize=20971520 -XX:MaxHeapSize=20971520 -XX:SurvivorRatio=8  -XX:MaxTenuringThreshold=15 -XX:PretenureSizeThreshold=3145728 -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:demo5gc.log
 * @author lizy4
 * @date 2021/9/9 14:42
 */
public class Demo5 {
    public static void main(String[] args) {
        byte[] arry1 = new byte[4*1024*1024];
        arry1 = null;

        byte[] arry2 = new byte[2*1024*1024];
        byte[] arry3 = new byte[2*1024*1024];
        byte[] arry4 = new byte[2*1024*1024];

        byte[] arry5 = new byte[128*1024];

        byte[] arry6 = new byte[2*1024*1024];
    }
}
