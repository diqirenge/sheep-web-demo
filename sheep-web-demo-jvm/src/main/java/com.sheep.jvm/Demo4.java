package com.sheep.jvm;

/**
 * 模拟对象进入老年代2
 * -XX:NewSize=10485760 -XX:MaxNewSize=10485760 -XX:InitialHeapSize=20971520 -XX:MaxHeapSize=20971520 -XX:SurvivorRatio=8  -XX:MaxTenuringThreshold=15 -XX:PretenureSizeThreshold=10485760 -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:demo4gc.log
 * @author lizy4
 * @date 2021/9/9 14:42
 */
public class Demo4 {
    public static void main(String[] args) {
        // 创建一个2M的数组
        byte[] arry1 = new byte[2*1024*1024];
        // 创建一个2M的数组
        arry1 = new byte[2*1024*1024];
        // 创建一个2M的数组
        arry1 = new byte[2*1024*1024];

        // 创建一个128k数组
        byte[] arry2 = new byte[128*1024];
        arry2 = null;

    }
}
