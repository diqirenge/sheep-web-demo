package com.sheep.jvm;

/**
 * -XX:NewSize=104857600 -XX:MaxNewSize=104857600 -XX:InitialHeapSize=209715200 -XX:MaxHeapSize=209715200 -XX:SurvivorRatio=8  -XX:MaxTenuringThreshold=15 -XX:PretenureSizeThreshold=3145728 -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:gc.log
 *
 * -XX:NewSize=104857600   // 新生代初始化内存的大小
 * -XX:MaxNewSize=104857600 // 新生代可被分配的内存的最大上限
 * -XX:InitialHeapSize=209715200 // 初始堆大小
 * -XX:MaxHeapSize=209715200 // 最大堆大小
 * -XX:SurvivorRatio=8 // survivor比例
 * -XX:MaxTenuringThreshold=15 //控制新生代需要经历多少次GC晋升到老年代中的最大阈值
 * -XX:PretenureSizeThreshold=3145728 // 任何比这个大的对象都不会尝试在新生代分配，将在老年代分配内存
 * -XX:+UseParNewGC // 使用ParNew垃圾回收器
 * -XX:+UseConcMarkSweepGC //使用CMS垃圾回收器
 * -XX:+PrintGCDetails // 打印GC详细信息
 * -XX:+PrintGCTimeStamps // 打印GC次数
 * -Xloggc:gc.log // 输出gc日志
 *
 * @author lizy4
 * @className Demo1
 * @date 2021/7/5 11:17
 * @description
 */
public class Demo1 {
    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(30000);
        while (true){
            loadData();
        }
    }

    private static void loadData() throws InterruptedException {
        byte[] data = null;
        for (int i=0;i<=50;i++){
            data = new byte[100 * 1024];
        }
        data = null;
        Thread.sleep(1000);
    }
}
