package com.sheep.jvm;

/**
 * -XX:NewSize=5242880 -XX:MaxNewSize=5242880 -XX:InitialHeapSize=10485760 -XX:MaxHeapSize=10485760 -XX:SurvivorRatio=8 -XX:PretenureSizeThreshold=10485760 -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:demo2gc.log
 * <p>
 * -XX:NewSize=104857600   // 新生代初始化内存的大小
 * -XX:MaxNewSize=104857600 // 新生代可被分配的内存的最大上限
 * -XX:InitialHeapSize=209715200 // 初始堆大小
 * -XX:MaxHeapSize=209715200 // 最大堆大小
 * -XX:SurvivorRatio=8 // survivor比例
 * -XX:MaxTenuringThreshold=15 //控制新生代需要经历多少次GC晋升到老年代中的最大阈值
 * -XX:PretenureSizeThreshold=3145728 // 任何比这个大的对象都不会尝试在新生代分配，将在老年代分配内存
 * -XX:+UseParNewGC // 使用ParNew垃圾回收器
 * -XX:+UseConcMarkSweepGC //使用CMS垃圾回收器
 * -XX:+PrintGCDetails // 打印GC详细信息
 * -XX:+PrintGCTimeStamps // 打印GC次数
 * -Xloggc:gc.log // 输出gc日志
 *
 * @author lizy
 * @date 2021/09/07
 */
public class Demo2 {
    public static void main(String[] args){
        // 1、eden，数组①存放1MB数据
        byte[] array1 = new byte[1024 * 1024];
        // 2、eden，数组②存放1MB数据，数组①没有对象引用，则成了垃圾对象
        array1 = new byte[1024 * 1024];
        // 3、eden，数组③存放1MB数据，数组①②没有对象引用，则成了垃圾对象
        array1 = new byte[1024 * 1024];
        // 4、array1这个对象没有指向了，数组①②③，都变成了垃圾对象
        array1 = null;
        // 5、eden，数组④存放2MB数据，但是eden总共4MB，现在占了3MB，再想放2MB就放不下了，就YoungGC了
        byte[] array2 = new byte[2 * 1024 * 1024];
        // 6、查看gc日志，demo2gc.log
    }

}
