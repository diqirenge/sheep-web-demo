package com.sheep.jvm;

/**
 * 模拟OOM，栈内存溢出的场景
 * -XX:ThreadStackSize=1m
 * @author lizy
 * @date 2021/09/13
 */
public class Demo8 {
    public static long counter = 0;
    public static void main(String[] args){
        work();
    }

    private static void work() {
        System.out.println("目前是第"+(++counter)+"次调用方法");
        work();
    }
}
