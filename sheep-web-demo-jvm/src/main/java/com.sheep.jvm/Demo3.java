package com.sheep.jvm;

/**
 * 模拟对象进入老年代
 * -XX:NewSize=10485760 -XX:MaxNewSize=10485760 -XX:InitialHeapSize=20971520 -XX:MaxHeapSize=20971520 -XX:SurvivorRatio=8  -XX:MaxTenuringThreshold=15 -XX:PretenureSizeThreshold=10485760 -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:demo3gc.log
 * @author lizy4
 * @date 2021/9/9 14:42
 */
public class Demo3 {
    public static void main(String[] args) {
        // 创建一个2M的数组
        byte[] arry1 = new byte[2*1024*1024];
        // 创建一个2M的数组
        arry1 = new byte[2*1024*1024];
        // 创建一个2M的数组
        arry1 = new byte[2*1024*1024];
        // eden区，6M数据没有被引用
        arry1 = null;

        // 创建一个128k数组
        byte[] arry2 = new byte[128*1024];
        // 创建一个2M数组（eden总共才6M，这里肯定放不下，所以肯定会YoungGC）
        byte[] arry3 = new byte[2*1024*1024];

        // 再创建2个2M数组，和128K的数组
        arry3 = new byte[2*1024*1024];
        arry3 = new byte[2*1024*1024];
        arry3 = new byte[128*1024];
        arry3 = null;

        // 创建2M数组，再次YoungGC
        byte[] arry4 = new byte[2*1024*1024];
    }
}
