package com.sheep.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * 模拟OOM，堆内存溢出的场景
 * -Xms10m -Xmx10m
 * @author lizy
 * @date 2021/09/13
 */
public class Demo9 {
    public static void main(String[] args){
        long counter = 0;
        List<Object> list = new ArrayList<>();
        while (true){
            list.add(new Object());
            System.out.println("当前创建了第"+(++counter)+"个对象");
        }
    }
}
