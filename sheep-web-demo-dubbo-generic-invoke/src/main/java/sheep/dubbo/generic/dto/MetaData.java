package sheep.dubbo.generic.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MetaData {

    private String serviceName;

    private String interfaceName;

    private String version;

    private String method;

    private String parameterTypes;

    private String args;

}