package com.sheep.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sheep.mybatis.plus.entity.User;
import org.springframework.stereotype.Component;

/**
 * Usage                : 用户映射器
 * Project name         :
 * Author               : lizy4
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/01/14
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/01/14       lizy4             1.0.0             新建
 *
 */
@Component
public interface UserMapper extends BaseMapper<User> {
}
