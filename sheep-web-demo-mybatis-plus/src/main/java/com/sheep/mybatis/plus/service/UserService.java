package com.sheep.mybatis.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sheep.mybatis.plus.entity.User;

/**
 * Usage                : 用户服务
 * Project name         :
 * Author               : lizy4
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/01/14
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/01/14       lizy4             1.0.0             新建
 *
 */
public interface UserService extends IService<User> {
    void mytest() throws InterruptedException;
    void mytest1(User user) throws InterruptedException;
}
