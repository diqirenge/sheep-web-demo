package com.sheep.junit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sheep.junit.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户映射器
 *
 * @author lizy
 * @date 2021/08/09
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 查询所有用户
     * @return 用户信息
     */
    List<User> listUsers();

    /**
     * 根据ID查询用户
     * @param id 用户ID
     * @return 用户信息
     */
    User getUserById(@Param("id") Long id);

    /**
     * 新增用户
     * @param user 用户信息
     */
    void saveUser(User user);

    /**
     * 更新用户
     * @param user 用户信息
     */
    Boolean updateUser(User user);

    /**
     * 删除用户
     * @param id 用户ID
     */
    Boolean removeUser(@Param("id") Long id);
}
