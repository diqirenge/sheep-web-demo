package com.sheep.junit.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sheep.junit.entity.User;
import com.sheep.junit.mapper.UserMapper;
import com.sheep.junit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户服务impl
 *
 * @author lizy
 * @date 2021/08/09
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> listUsers() {
        return userMapper.listUsers();
    }

    @Override
    public User getUserById(Long id) {
        return userMapper.getUserById(id);
    }

    @Override
    public Long saveUser(User user) {
        userMapper.saveUser(user);
        return user.getId();
    }

    @Override
    public Boolean updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public Boolean removeUser(Long id) {
        return userMapper.removeUser(id);
    }
}
