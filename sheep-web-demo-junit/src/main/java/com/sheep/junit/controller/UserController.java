package com.sheep.junit.controller;

import com.sheep.junit.entity.User;
import com.sheep.junit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 查询所有用户
     * @return 用户信息
     *
     * 这个@GetMapping注解表示的就是，这个接口仅仅接收GET类型的http请求
     *
     */
    @GetMapping("/")
    public List<User> listUsers() {
        return userService.listUsers();
    }

    /**
     * 根据ID查询用户
     * @param id 用户ID
     * @return 用户信息
     *
     * {id}，就是通过占位符的方式，可以让我们提取请求URL中的参数
     * 表明说，我要定位的一个资源是id=3的一个用户
     *
     */
    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") Long id) {
        return userService.getUserById(id);
    }

    /**
     * 新增用户
     * @param user 用户信息
     */
    @PostMapping("/")
    public String saveUser(@RequestBody @Valid User user) {
        Long id = userService.saveUser(user);
        return "success";
    }

    /**
     * 更新用户
     * @param user 用户信息
     */
    @PutMapping("/{id}")
    public String updateUser(@RequestBody User user) {
        userService.updateUser(user);
        return "success";
    }

    /**
     * 删除用户
     * @param id 用户ID
     */
    @DeleteMapping("/{id}")
    public String removeUser(@PathVariable("id") Long id) {
        userService.removeUser(id);
        return "success";
    }
}
