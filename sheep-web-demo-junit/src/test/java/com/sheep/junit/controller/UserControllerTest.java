package com.sheep.junit.controller;

import com.alibaba.fastjson.JSONObject;
import com.sheep.junit.entity.User;
import com.sheep.junit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


/**
 * 用户控制器测试
 *
 * @author lizy
 * @date 2021/08/09
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UserControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private UserController userController;

	@MockBean
	private UserService userService;

	/**
	 * 前置方法，一般执行初始化代码
	 */
	@Before
	public void setup() {

		MockitoAnnotations.initMocks(this);

		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}
	
	/**
	 * 测试用例：查询所有用户信息
	 */
	@Test
	public void testListUsers() {
		try {
			List<User> users = new ArrayList<User>();
			
			User user = new User();
			user.setId(1L);
			user.setName("测试用户");  
			user.setAge(20);
			
			users.add(user);
			
			when(userService.listUsers()).thenReturn(users);
			
			mockMvc.perform(get("/user/"))
					.andExpect(content().json(JSONArray.toJSONString(users))); 
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}
	
	/**
	 * 测试用例：根据ID查询一个用户
	 */
	@Test
	public void testGetUserById() {
		try {
			Long userId = 1L;
			
			User user = new User();
			user.setId(userId);
			user.setName("测试用户");  
			user.setAge(20);
			
			when(userService.getUserById(userId)).thenReturn(user);
			
			mockMvc.perform(get("/user/{id}", userId))  
					.andExpect(content().json(JSONObject.toJSONString(user)));
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}
	
	/**
	 * 测试用例：新增用户
	 */
	@Test
	public void testSaveUser() {
		Long userId = 1L;
		
		User user = new User();
		user.setName("测试用户");  
		user.setAge(20);
		
		when(userService.saveUser(user)).thenReturn(userId);
		
		try {
			mockMvc.perform(post("/user/").contentType("application/json").content(JSONObject.toJSONString(user)))
					.andExpect(content().string("success"));
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}
	
	/**
	 * 测试用例：修改用户
	 */
	@Test
	public void testUpdateUser() {
		Long userId = 1L;
		
		User user = new User();
		user.setId(userId); 
		user.setName("测试用户");  
		user.setAge(20);
		
		when(userService.updateUser(user)).thenReturn(true);
		
		try {
			mockMvc.perform(put("/user/{id}", userId).contentType("application/json").content(JSONObject.toJSONString(user)))  
					.andExpect(content().string("success"));     
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}
	
	/**
	 * 测试用例：删除用户
	 */
	@Test
	public void testRemoveUser() {
		Long userId = 1L;

		when(userService.removeUser(userId)).thenReturn(true);  
		
		try {
			mockMvc.perform(delete("/user/{id}", userId))   
					.andExpect(content().string("success"));     
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}
	
}
