package com.sheep.junit.mapper;

import com.sheep.junit.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class UserMapperTest {

    /**
     * 持久层，不需要使用模拟对象
     */
    @Autowired
    private UserMapper userMapper;

    /**
     * 测试用例：查询所有用户信息
     */
    @Test
    public void testListUsers() {
        // 初始化数据
        initUser(20);
        // 调用方法
        List<User> resultUsers = userMapper.listUsers();
        // 断言不为空
        assertNotNull(resultUsers);
        // 断言size大于0
        Assert.assertThat(resultUsers.size(), is(greaterThanOrEqualTo(0)));
    }

    /**
     * 测试用例：根据ID查询一个用户
     */
    @Test
    public void testGetUserById() {
        // 初始化数据
        User user = initUser(20);
        Long userId = user.getId();
        // 调用方法
        User resultUser = userMapper.getUserById(userId);
        // 断言对象相等
        assertEquals(user.toString(), resultUser.toString());
    }

    /**
     * 测试用例：新增用户
     */
    @Test
    public void testSaveUser() {
        initUser(20);
    }

    /**
     * 测试用例：修改用户
     */
    @Test
    public void testUpdateUser() {
        // 初始化数据
        Integer oldAge = 20;
        Integer newAge = 21;
        User user = initUser(oldAge);
        user.setAge(newAge);
        // 调用方法
        Boolean updateResult = userMapper.updateUser(user);
        // 断言是否为真
        assertTrue(updateResult);
        // 调用方法
        User updatedUser = userMapper.getUserById(user.getId());
        // 断言是否相等
        assertEquals(newAge, updatedUser.getAge());
    }

    /**
     * 测试用例：删除用户
     */
    @Test
    public void testRemoveUser() {
        // 初始化数据
        User user = initUser(20);
        // 调用方法
        Boolean removeResult = userMapper.removeUser(user.getId());
        // 断言是否为真
        assertTrue(removeResult);
    }

    private User initUser(int i) {
        // 初始化数据
        User user = new User();
        user.setName("测试用户");
        user.setAge(i);
        // 调用方法
        userMapper.saveUser(user);
        // 断言id不为空
        assertNotNull(user.getId());
        return user;
    }
}
