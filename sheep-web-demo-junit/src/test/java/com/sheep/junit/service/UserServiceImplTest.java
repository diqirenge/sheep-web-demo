package com.sheep.junit.service;

import com.sheep.junit.entity.User;
import com.sheep.junit.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * 用户服务impl测试
 *
 * @author lizy
 * @date 2021/08/09
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

	@Autowired
	private UserService userService;

	/**
	 * 这个注解表名，该对象是个mock对象，他将替换掉你@Autowired标记的对象
	 */
	@MockBean
	private UserMapper userMapper;
	
	/**
	 * 测试用例：查询所有用户信息
	 */
	@Test
	public void testListUsers() {
		// 初始化数据
		List<User> users = new ArrayList<>();

		User user = initUser(1L);
		
		users.add(user);
		// mock行为
		when(userMapper.listUsers()).thenReturn(users);
		// 调用方法
		List<User> resultUsers = userService.listUsers();
		// 断言是否相等
		assertEquals(users, resultUsers);
	}
	
	/**
	 * 测试用例：根据ID查询一个用户
	 */
	@Test
	public void testGetUserById() {
		// 初始化数据
		Long userId = 1L;

		User user = initUser(userId);
		// mock行为
		when(userMapper.getUserById(userId)).thenReturn(user);
		// 调用方法
		User resultUser = userService.getUserById(userId);
		// 断言是否相等
		assertEquals(user, resultUser);

	}
	
	/**
	 * 测试用例：新增用户
	 */
	@Test
	public void testSaveUser() {
		// 初始化数据
		User user = initUser(1L);
		// 默认的行为（这一行可以不写）
		doNothing().when(userMapper).saveUser(any());
		// 调用方法
		userService.saveUser(user);
		// 验证执行次数
		verify(userMapper, times(1)).saveUser(user);

	}

	/**
	 * 测试用例：修改用户
	 */
	@Test
	public void testUpdateUser() {
		// 初始化数据
		User user = initUser(1L);
		// 模拟行为
		when(userMapper.updateUser(user)).thenReturn(true);
		// 调用方法
		Boolean updateResult = userService.updateUser(user);
		// 断言是否为真
		assertTrue(updateResult); 
	}

	/**
	 * 测试用例：删除用户
	 */
	@Test
	public void testRemoveUser() {
		Long userId = 1L;
		// 模拟行为
		when(userMapper.removeUser(userId)).thenReturn(true);
		// 调用方法
		Boolean removeResult = userService.removeUser(userId);
		// 断言是否为真
		assertTrue(removeResult);
	}

	private User initUser(Long userId) {
		User user = new User();
		user.setName("测试用户");
		user.setAge(20);
		user.setId(userId);
		return user;
	}
	
}
