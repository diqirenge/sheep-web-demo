CREATE DATABASE `dynamic` ;

USE `dynamic`;
-- ----------------------------
-- Table structure for data_source_meta
-- ----------------------------
DROP TABLE IF EXISTS `data_source_meta`;
CREATE TABLE `data_source_meta`  (
                                 `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
                                 `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
                                 `url` varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'mysql地址',
                                 `mysql_schema` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'mysql库名',
                                 `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'mysql用户名',
                                 `user_password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'mysql密码',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_source_meta
-- ----------------------------
INSERT INTO `data_source_meta` VALUES (2, '内置测试库', 'localhost:3306', 'dy_test', 'root', '123456');