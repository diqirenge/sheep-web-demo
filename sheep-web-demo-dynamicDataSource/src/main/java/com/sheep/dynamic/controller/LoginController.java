package com.sheep.dynamic.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 登录控制器
 *
 * @author 第七人格
 * @date 2023/04/13
 */
@RestController
@RequestMapping(value = "/admin")
public class LoginController {
    /**
     * 登录
     *
     * @param schema  模式
     * @param request 请求
     * @return {@link String}
     */
    @GetMapping("/login/{schema}")
    public String login(@PathVariable String schema, HttpServletRequest request) {
        // 存入session，用于切库
        request.getSession().setAttribute("schema",schema);
        return "登录成功！";
    }
}
