package com.sheep.dynamic.controller;

import com.sheep.dynamic.entity.Area;
import com.sheep.dynamic.service.AreaServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 区域控制器
 *
 * @author 第七人格
 * @date 2023/04/13
 */
@RestController
@RequestMapping(value = "/area")
public class AreaController {
    /**
     * 区域服务
     */
    @Resource
    private AreaServiceImpl areaService;

    /**
     * 选择所有
     *
     * @param Area 区域
     * @return {@link Object}
     */
    @PostMapping("/selectAll")
    public Object selectAll(@RequestBody Area Area){
        return areaService.selectAll(Area);
    }
}
