package com.sheep.dynamic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sheep.dynamic.entity.DataSourceMeta;

/**
 * 数据源元映射器
 *
 * @author 第七人格
 * @date 2023/04/13
 */
public interface DataSourceMetaMapper extends BaseMapper<DataSourceMeta> {
}
