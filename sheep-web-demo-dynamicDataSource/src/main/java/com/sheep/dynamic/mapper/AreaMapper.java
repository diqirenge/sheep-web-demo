package com.sheep.dynamic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sheep.dynamic.entity.Area;

/**
 * 区域映射器
 *
 * @author 第七人格
 * @date 2023/04/13
 */
public interface AreaMapper extends BaseMapper<Area> {

}
