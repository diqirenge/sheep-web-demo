package com.sheep.dynamic.service;

import com.baomidou.dynamic.datasource.annotation.DS;

/**
 * saas服务
 * 该接口下的所有数据操作默认根据session中的schema进行路由。
 *
 * @author 第七人格
 * @date 2023/04/13
 */
@DS("#session.schema")
public class SaasService {
}