package com.sheep.dynamic.service;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.sheep.dynamic.entity.Area;
import com.sheep.dynamic.mapper.AreaMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 区域服务impl
 *
 * @author 第七人格
 * @date 2023/04/13
 */
@Service
public class AreaServiceImpl extends SaasService {
    /**
     * 区域映射器
     */
    @Resource
    private AreaMapper areaMapper;

    /**
     * 选择所有
     *
     * @param area 区域
     * @return {@link List}<{@link Area}>
     */
    public List<Area> selectAll(Area area) {
        return new LambdaQueryChainWrapper<>(areaMapper)
                .eq(Area::getAreaId, area.getAreaId())
                .list();
    }
}
