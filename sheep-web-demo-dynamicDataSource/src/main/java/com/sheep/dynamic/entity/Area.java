package com.sheep.dynamic.entity;

/**
 * 区域
 *
 * @author 第七人格
 * @date 2023/04/13
 */
public class Area {
    /**
     * 区域id
     */
    private Integer areaId;
    /**
     * 区域名称
     */
    private String areaName;

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
