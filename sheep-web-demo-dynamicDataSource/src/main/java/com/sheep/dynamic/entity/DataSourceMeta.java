package com.sheep.dynamic.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据源元
 *
 * @author 第七人格
 * @date 2023/04/13
 */
@TableName("data_source_meta")
public class DataSourceMeta {
    /**
     * id
     */
    @TableId("id")
    private Integer id;
    /**
     * 名字
     */
    @TableField("name")
    private String name;
    /**
     * url
     */
    @TableField("url")
    private String url;
    /**
     * mysql模式
     */
    @TableField("mysql_schema")
    private String mysqlSchema;
    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;
    /**
     * 密码
     */
    @TableField("user_password")
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMysqlSchema() {
        return mysqlSchema;
    }

    public void setMysqlSchema(String mysqlSchema) {
        this.mysqlSchema = mysqlSchema;
    }

    public String getUsername() {
        return userName;
    }

    public void setUsername(String username) {
        this.userName = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
