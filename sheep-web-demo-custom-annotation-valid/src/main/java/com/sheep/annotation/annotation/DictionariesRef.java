package com.sheep.annotation.annotation;

import com.sheep.annotation.validator.DictionariesValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * <p>字典校验注解</p>
 *
 * @author 第七人格
 * @date 2022/12/09
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DictionariesValidator.class)
public @interface DictionariesRef {

    /**
     * 引用的字典对象，该对象必须包含isValid的静态方法。
     *
     * @return 校验目标类。
     */
    Class<?> constDictClass();

    /**
     * 错误消息提示。
     *
     * @return 错误提示。
     */
    String message() default "无效的字典引用值！";

    /**
     * 验证分组。
     *
     * @return 验证分组。
     */
    Class<?>[] groups() default {};

    /**
     * 载荷对象类型。
     *
     * @return 载荷对象。
     */
    Class<? extends Payload>[] payload() default {};

}
