package com.sheep.annotation.dto;

import com.sheep.annotation.validator.CardTypeEnum;
import com.sheep.annotation.annotation.DictionariesRef;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>入参</p>
 *
 * @author 第七人格
 * @date 2022/12/09
 */
@Data
public class DemoReq implements Serializable {

    /**
     * 证件类型
     * 
     * 注解DictionariesRef 
     * 第一个参数 constDictClass 传入我们的校验枚举类，此枚举实现了isValid方法
     * 第二个参数 message 传入我们自定义的错误信息
     */
    @NotBlank(message = "证件类型不能为空")
    @DictionariesRef(constDictClass = CardTypeEnum.class, message = "证件类型非法！")
    private String cardType;
}
