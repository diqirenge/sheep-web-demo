package com.sheep.annotation.validator;

import lombok.Getter;

/**
 * <p>证件类型枚举</p>
 *
 * @author 第七人格
 * @date 2022/12/09
 */
@Getter
public enum CardTypeEnum {
    ID_CARD("I000001","身份证"),
    ;
    private String code;
    private String desc;

    CardTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 得到枚举
     *
     * @param code 代码
     * @return {@link CardTypeEnum}
     */
    public static CardTypeEnum getEnum(String code) {
        for (CardTypeEnum ge : values()) {
            if(ge.getCode().equals(code)) {return ge;}
        }
        return null;
    }

    /**
     * 得到详情描述
     *
     * @param code 代码
     * @return {@link String}
     */
    public static String getDesc(String code) {
        for (CardTypeEnum ge : values()) {
            if(ge.getCode().equals(code)) {return ge.getDesc();}
        }
        return null;
    }

    /**
     * 是有效
     *
     * true 表示有效
     * false 表示无效
     *
     * @param code 代码
     * @return boolean
     */
    public static boolean isValid(String code) {
        return getEnum(code) != null;
    }
}
