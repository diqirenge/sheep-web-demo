package com.sheep.annotation.validator;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.sheep.annotation.annotation.DictionariesRef;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Method;


/**
 * <p>字典验证器-实现类</p>
 *
 * @author 第七人格
 * @date 2022/12/09
 */
public class DictionariesValidator implements ConstraintValidator<DictionariesRef, Object> {

    /**
     * 注解
     */
    private DictionariesRef dictionariesRef;

    /**
     * 重写初始化方法
     *
     * @param dictionariesRef 字典校验器注解
     */
    @Override
    public void initialize(DictionariesRef dictionariesRef) {
        this.dictionariesRef = dictionariesRef;
    }

    /**
     * 重写校验方法
     *
     * true 表示校验通过
     * false 表示校验不通过
     *
     * @param object                          待校验对象
     * @param constraintValidatorContext 字典验证器上下文
     * @return boolean
     */
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        // 校验对象为空，则默认校验通过
        if (ObjectUtil.isEmpty(object)) {
            return true;
        }
        // 通过反射获取方法。划重点，这里也是为什么，待校验对象需要实现isValid方法的原因
        Method method =
                ReflectUtil.getMethodByName(dictionariesRef.constDictClass(), "isValid");
        return ReflectUtil.invokeStatic(method, object);
    }
}
