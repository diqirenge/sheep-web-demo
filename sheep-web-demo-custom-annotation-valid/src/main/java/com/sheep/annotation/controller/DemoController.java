package com.sheep.annotation.controller;

import com.sheep.annotation.dto.DemoReq;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>控制层demo</p>
 *
 * @author 第七人格
 * @date 2022/12/09
 */
@RestController
@RequestMapping("/demo")
public class DemoController {
    
    @PostMapping(value = "/v1/apply")
    public void apply(@Valid @RequestBody DemoReq req){
		// todo
    }

}
