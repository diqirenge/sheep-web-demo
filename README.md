# sheep-boot-demo

#### 介绍
>后端代码demo，记录工作学习常用demo，包含但不限于各种框架的集成以及优秀代码的记录。

#### 示例目录

[1、SpringBoot-helloworld示例](./sheep-web-demo-helloworld)

[2、Spring集成logback示例](./sheep-web-demo-logback)

[3、SpringBoot集成swagger示例](./sheep-web-demo-swagger)

[4、SpringBoot启动logo替换示例](./sheep-web-demo-bootlogo)

[5、SpringBoot-aop示例](./sheep-web-demo-aop)

[6、SpringBoot-统一异常处理示例](./sheep-web-demo-gloabexception)

[7、SpringBoot集成actuator，检查项目运行情况示例](./sheep-web-demo-actuator)

[8、SpringBoot集成admin，项目监控示例](./sheep-web-demo-admin)

[9、SpringBoot集成mybatis-plus示例](./sheep-web-demo-mybatis-plus)

[10、SpringBoot集成xxl-job示例](./sheep-web-demo-xxl-job)

[11、SpringBoot集成spring-scheduled示例](./sheep-web-demo-spring-scheduled)

[12、SpringBoot集成spring-retry示例](./sheep-web-demo-spring-retry)

[13、SpringBoot集成rabbitmq示例](./sheep-web-demo-rabbitmq)

[14、SpringBoot集成redis示例](./sheep-web-demo-redis)

[15、SpringBoot集成mapstruct示例](./sheep-web-demo-convertor)

[16、SpringBoot获取Spring上下文示例](./sheep-web-demo-spring-context)

[17、SpringBoot集成spring-security示例](./sheep-web-demo-spring-security)

[18、简单的代码生成器](./sheep-web-demo-generator)

[19、jvm例子](./sheep-web-demo-jvm)

[20、Spring Boot 中使用aop+注解实现加解密、验签功能](./sheep-web-demo-aop2security)

[21、Spring Boot单元测试示例](./sheep-web-demo-junit)

[22、Spring Boot实现增强的@RequestBody注解](./sheep-web-demo-pwoerRequestBody)

[23、Spring Boot集成dubbo](./sheep-web-demo-dubbo-boot)

[24、Spring Cloud集成dubbo](./sheep-web-demo-dubbo-cloud)

[25、Spring boot自定义start，以swagger为例
（引用例子，参考1、SpringBoot-helloworld示例）
](./sheep-web-demo-customize-start)

[26、新增自定义线程池工具类实现](./sheep-web-demo-taskpool)

[27、集成dubbo+nacos服务注册与发现demo](./sheep-web-demo-nacos-discovery)

[28、集成nacos配置中心](./sheep-web-demo-nacos-config)

[29、集成Sentinel，修改sentinel-dashboard源码，接入nacos配置中心实现持久化](./sheep-web-demo-sentinel)

[30、集成webflux小例](./sheep-web-demo-webflux)

[31、自定义类加载器](./sheep-web-demo-custom-classloader)

[32、参考@NotBlank实现自定义注解校验器](./sheep-web-demo-custom-annotation-valid)

[33、应用启动好后，自动启动浏览器](./sheep-web-demo-swagger/src/main/java/com/sheep/swagger/config/IndexConfig.java)

[34、dubbo泛化调用小例](./sheep-web-demo-dubbo-generic-invoke)

[35、动态数据源示例](./sheep-web-demo-dynamicDataSource)

[36、checkstyle使用例子](./sheep-web-demo-checkstyle)


#### 关于作者

[第七人格的博客](http://www.52javaee.com)

#### 发版记录

##### 2021-08-23 
* 修改：完善README
##### 2021-08-26
* 新增：集成dubbo（注册中心使用zookeeper，springboot版）
##### 2021-08-29
* 新增：集成dubbo（注册中心使用zookeeper，springcloud版）
##### 2021-09-07
* 新增：新增JVM-demo2
##### 2021-09-09
* 新增：新增JVM-demo3、JVM-demo4、JVM-demo5、JVM-demo6
##### 2021-09-10
* 新增：新增JVM-demo7，模拟OOM，Metaspace内存溢出的场景
##### 2021-09-13
* 新增：新增JVM-demo8、JVM-demo9
##### 2021-09-15
* 新增：新增自定义sheep-web-demo-customize-start 
##### 2021-09-23
* 新增：新增自定义线程池工具类实现 
##### 2021-12-01
* 新增：集成dubbo+nacos服务注册与发现demo
##### 2021-12-02
* 新增：集成nacos配置中心
##### 2021-12-07
* 新增：集成Sentinel，修改sentinel-dashboard源码，接入nacos配置中心实现持久化
##### 2021-12-15
* 新增：集成webflux小例
##### 2023-03-03
* 新增：自定义类加载器，加载jar[31、自定义类加载器](./sheep-web-demo-custom-classloader)，[待加载的jar的例子](./sheep-web-demo-custom-classloader-jar)
##### 2023-03-03
* 新增：参考@NotBlank实现自定义注解校验器
##### 2023-03-07
* 新增：应用启动好后，自动启动浏览器
##### 2023-03-31
* 新增：dubbo泛化调用小例
##### 2023-04-13
* 新增：动态数据源示例
##### 2023-04-17
* 新增：checkstyle使用例子

