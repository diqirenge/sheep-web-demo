package com.sheep.scheduled;

import cn.hutool.core.date.DateUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

// 交给Spring容器管理
@Component
public class PushJob {

    // 推送方法，每秒执行一次，也可以使用corn表达式
    @Scheduled(fixedRate = 1000)
    public void push() throws InterruptedException {
        //休眠2秒，模拟耗时操作
        TimeUnit.SECONDS.sleep(2);
        System.out.println(Thread.currentThread().getName() + " push 模拟推送消息，" + DateUtil.now());
    }

}