package com.sheep.nacos;


/**
 * 对外暴露的接口
 *
 * @author 第七人格
 * @date 2021/12/01
 */
public interface IHelloService {
    String sayHello(String name);
}
