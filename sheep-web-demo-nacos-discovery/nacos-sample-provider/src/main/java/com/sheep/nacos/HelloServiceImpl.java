package com.sheep.nacos;

import org.apache.dubbo.config.annotation.Service;


/**
 * dubbo服务实现
 *
 * @author 第七人格
 * @date 2021/12/01
 */
@Service
public class HelloServiceImpl implements IHelloService {

    @Override
    public String sayHello(String s) {
        return "Hello World:"+s;
    }
}
