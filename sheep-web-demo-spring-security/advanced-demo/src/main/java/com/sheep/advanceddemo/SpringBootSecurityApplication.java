package com.sheep.advanceddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EnableGlobalMethodSecurity(
        jsr250Enabled = true, //JSR-250注解
        prePostEnabled = true, //spring表达式注解
        securedEnabled = true //SpringSecurity注解，推荐使用
)
public class SpringBootSecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityApplication.class, args);
    }

}
