package com.sheep.advanceddemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
    @RequestMapping("/main")
    public String main() {
        return "main";
    }

    //跳转到登录页的方法
    @RequestMapping("/toLogin")
    public String toLogin() {
        return "login";
    }

    //跳转到错误页的方法
    @RequestMapping("/to403")
    public String to403() {
        return "error/403";
    }
}