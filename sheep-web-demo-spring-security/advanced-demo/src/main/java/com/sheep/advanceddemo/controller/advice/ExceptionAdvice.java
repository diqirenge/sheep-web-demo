package com.sheep.advanceddemo.controller.advice;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionAdvice {
    //别导错类了：org.springframework.security.access.AccessDeniedException
    //只有出现AccessDeniedException异常才调转403.html页面
    @ExceptionHandler(AccessDeniedException.class)
    public String exceptionAdvice() {
        return "forward:/to403";
    }
}
