package com.sheep.advanceddemo.mapper;

import com.sheep.advanceddemo.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysRoleMapper {
    //保存一个角色
    void save(SysRole sysRole);

    //查询所有角色
    List<SysRole> findAll();

    //根据用户编号查询
    List<SysRole> findByUid(Integer uid);
}