package com.sheep.advanceddemo.controller;

import com.sheep.advanceddemo.entity.SysRole;
import com.sheep.advanceddemo.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/role")
public class SysRoleController {
    @Autowired
    private SysRoleService sysRoleService;

    @RequestMapping("/add")
    public String add() {
        return "sys-role/add";
    }

    @RequestMapping("/save")
    public String save(SysRole sysRole) {
        sysRoleService.save(sysRole);
        return "redirect:/role/findAll";
    }

    @RequestMapping("/findAll")
    public String findAll(Model model) {
        List<SysRole> sysRoles = sysRoleService.findAll();
        model.addAttribute("sysRoles", sysRoles);
        return "sys-role/list";
    }
}