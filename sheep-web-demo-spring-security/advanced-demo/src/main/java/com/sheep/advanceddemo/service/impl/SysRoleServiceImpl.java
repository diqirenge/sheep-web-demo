package com.sheep.advanceddemo.service.impl;

import com.sheep.advanceddemo.entity.SysRole;
import com.sheep.advanceddemo.mapper.SysRoleMapper;
import com.sheep.advanceddemo.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SysRoleServiceImpl  implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public void save(SysRole sysRole) {
        sysRoleMapper.save(sysRole);
    }

    @Override
    public List<SysRole> findAll() {
        return sysRoleMapper.findAll();
    }

    @Override
    public List<SysRole> findByUid(Integer uid) {
        return sysRoleMapper.findByUid(uid);
    }
}