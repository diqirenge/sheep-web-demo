package com.sheep.advanceddemo.service;

import com.sheep.advanceddemo.entity.SysRole;
import com.sheep.advanceddemo.entity.SysUser;

import java.util.List;

public interface SysRoleService {
    //保存一个角色
    void save(SysRole sysRole);

    //查询所有角色
    List<SysRole> findAll();

    //根据用户编号查询
    List<SysRole> findByUid(Integer uid);
}