package com.sheep.advanceddemo.service;

import com.sheep.advanceddemo.entity.SysUser;

import java.util.List;
import java.util.Map;

public interface SysUserService {
    //保存一个用户
    void save(SysUser sysUser);

    //查询所有用户
    List<SysUser> findAll();

    //查询用户角色
    Map<String, Object> getRolesByUser(Integer uid);

    //添加用户角色
    void addRolesToUser(Integer uid, Integer[] rids);
}