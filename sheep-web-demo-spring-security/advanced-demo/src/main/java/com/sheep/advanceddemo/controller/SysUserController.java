package com.sheep.advanceddemo.controller;

import com.sheep.advanceddemo.entity.SysUser;
import com.sheep.advanceddemo.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("/add")
    public String add() {
        return "sys-user/add";
    }

    @RequestMapping("/save")
    public String save(SysUser sysUser) {
        sysUserService.save(sysUser);
        return "redirect:/user/findAll";
    }

    @RequestMapping("/findAll")
    public String findAll(Model model) {
        List<SysUser> sysUsers = sysUserService.findAll();
        model.addAttribute("sysUsers", sysUsers);
        return "sys-user/list";
    }

    @RequestMapping("/getRolesByUser")
    public String getRolesByUser(Model model, Integer uid) {
        Map<String, Object> map = sysUserService.getRolesByUser(uid);
        model.addAttribute("uid", uid);
        model.addAttribute("allRoles", map.get("allRoles"));
        model.addAttribute("hasRoles", map.get("hasRoles"));
        return "/sys-user/roles";
    }

    @RequestMapping("/addRolesToUser")
    public String getRolesByUser(Integer uid, Integer[] rids) {
        sysUserService.addRolesToUser(uid, rids);
        return "redirect:/user/getRolesByUser?uid=" + uid;
    }
}