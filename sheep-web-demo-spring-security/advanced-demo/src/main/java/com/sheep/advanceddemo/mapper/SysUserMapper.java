package com.sheep.advanceddemo.mapper;

import com.sheep.advanceddemo.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysUserMapper {
    //保存一个用户
    void save(SysUser sysUser);

    //查询所有用户
    List<SysUser> findAll();

    //根据用户编号查询出当前用户下的所有角色编号
    List<Integer> getRolesIdByUserId(Integer uid);

    //根据用户编号删除掉当前用户下的所有角色编号
    void removeRolesIdByUserId(Integer uid);

    //根据用户编号和角色编号添加一条用户角色关系
    void addRoleIdByUserId(@Param("uid") Integer uid, @Param("rid") Integer rid);

    //根据用户名称查询所对应的用户信息
    SysUser findUserByUsername(String username);
}