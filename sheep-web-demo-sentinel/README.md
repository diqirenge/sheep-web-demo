#sheep-web-demo-sentinel

> sentinel
# 源码修改
## SentinelDashboard
### 1、在[pom](../sentinel-dashboard/pom.xml)中把sentinel-datasource-nacos中的<scope>test</scope>注释掉



    <dependency>
        <groupId>com.alibaba.csp</groupId>
        <artifactId>sentinel-datasource-nacos</artifactId>
       <!-- <scope>test</scope>-->
    </dependency>
### 2、在[sidebar.html](../sentinel-dashboard/src/main/webapp/resources/app/scripts/directives/sidebar/sidebar.html)中去掉V1

    <li ui-sref-active="active" ng-if="!entry.isGateway">
    <a ui-sref="dashboard.flowV1({app: entry.app})">
      <i class="glyphicon glyphicon-filter"></i>&nbsp;&nbsp;流控规则</a>
    </li>

### 3、实现[Nacos](../sentinel-dashboard/src/main/java/com/alibaba/csp/sentinel/dashboard/rule/nacos)相关配置


### 4、修改注入的[实例](../sentinel-dashboard/src/main/java/com/alibaba/csp/sentinel/dashboard/controller/v2/FlowControllerV2.java)

    @Autowired
    @Qualifier("flowRuleNacosProvider")
    private DynamicRuleProvider<List<FlowRuleEntity>> ruleProvider;
    @Autowired
    @Qualifier("flowRuleNacosPublisher")
    private DynamicRulePublisher<List<FlowRuleEntity>> rulePublisher;


### 5、添加[Nacos配置信息](../sentinel-dashboard/src/main/resources/application.properties)
    sentinel.nacos.serverAddr=127.0.0.1:8848
    sentinel.nacos.namespace=
    sentinel.nacos.group-id=DEFAULT_GROUP





# 启动流程

1、启动nacos

访问地址http://localhost:8848/nacos

2、创建配置文件

* DataID：spring-cloud-sentinel-dynamic-sentinel-flow
* Group:DEFAULT_GROUP
* 格式：JSON
* 配置内容：
  [{"app":"spring-cloud-sentinel-dynamic","clusterMode":false,"controlBehavior":0,"count":1.0,"grade":1,"id":3,"limitApp":"default","resource":"/dynamic","strategy":0}]

3、启动SentinelDashboard
* 访问地址：http://localhost:7777
* 账号：sentinel
* 密码：sentinel

4、启动DynamicController

* [执行http调用](../sheep-web-demo-sentinel/src/main/http/http-test-api.http)

* 从Nacos同步的流控规则
  如果在SentinelDashboard修改规则，则会同步到Nacos，完成持久化
![img.png](img.png)