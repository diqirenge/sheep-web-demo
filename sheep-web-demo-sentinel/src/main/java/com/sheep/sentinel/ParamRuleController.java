package com.sheep.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRuleManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.websocket.server.PathParam;
import java.util.Collections;


/**
 * 控制器参数的规则
 *
 * @author 第七人格
 * @date 2021/12/07
 */
@RestController
public class ParamRuleController {

  @SentinelResource
  @GetMapping("/hello")
  public String sayHello(@PathParam("id")String id){
      return "access success";
  }
}
