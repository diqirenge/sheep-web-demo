package com.sheep.sentinel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamicRuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicRuleApplication.class, args);
	}

}
