package com.sheep.redis.redisson;

import com.sheep.redis.RedisApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author lizy4
 * @className JedisTest
 * @date 2021/2/22 14:13
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {RedisApplication.class})
@WebAppConfiguration
@Slf4j
public class RedissonTest {
    @Autowired
    private RedissonClient redissonClient;
}