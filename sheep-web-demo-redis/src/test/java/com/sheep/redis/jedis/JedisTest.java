package com.sheep.redis.jedis;

import com.sheep.redis.RedisApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author lizy4
 * @className JedisTest
 * @date 2021/2/22 14:13
 * @description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {RedisApplication.class})
@WebAppConfiguration
@Slf4j
public class JedisTest {
    @Autowired
    private JedisPool jedisPool;

    @Test
    public void set() {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            String returnVlaue = jedis.set("a", "1");
            log.info("set 返回值是：{}", returnVlaue);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Test
    public void del() {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            Long a = jedis.del("a");
            log.info("set 返回值是：{}", a);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }
}