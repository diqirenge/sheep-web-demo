package com.sheep.redis.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis-Jedis配置类。
 *
 * @author 第七人格
 * @date 2020-10-28
 */
@Configuration
@ConditionalOnProperty(name = "redis.jedis.enabled", havingValue = "true")
public class JedisConfig {

    /**
     * 端口
     */
    @Value("${redis.jedis.port}")
    private Integer port;

    /**
     * 主机地址
     */
    @Value("${redis.jedis.host}")
    private String redisHost;

    /**
     * 超时时间
     */
    @Value("${redis.jedis.timeout}")
    private int timeout;

    /**
     * 资源池最大连接数
     */
    @Value("${redis.jedis.pool.maxTotal}")
    private Integer maxTotal;

    /**
     * 资源池允许最大空闲连接数
     */
    @Value("${redis.jedis.pool.maxIdle}")
    private Integer maxIdle;

    /**
     * 资源池确保最少空闲连接数
     */
    @Value("${redis.jedis.pool.minIdle}")
    private Integer minIdle;

    /**
     * 	当资源连接池用尽后，调用者的最大等待时间（毫秒）
     */
    @Value("${redis.jedis.pool.maxWait}")
    private Integer maxWait;

    @Bean
    public JedisPool getJedisPool() {
        //Jedis配置信息
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(maxTotal);
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMinIdle(minIdle);
        jedisPoolConfig.setMaxWaitMillis(maxWait);
        jedisPoolConfig.setEvictorShutdownTimeoutMillis(2000);
        return new JedisPool(jedisPoolConfig, redisHost, port);
    }
}
