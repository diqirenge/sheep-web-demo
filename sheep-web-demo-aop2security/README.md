# spring-boot-demo-aop2security

> 本 demo 主要演示了如何在 Spring Boot 中使用aop+注解实现加解密、验签功能

# 一、楔子

最近小七接到一个任务，需要针对现有的一些接口，按照第三方的要求进行加解密、签名验签等升级。

# 二、改造思路

## 1、硬编码

直接改变以前方法的签名和结果，并且在方法里耦合加解密验签等逻辑

以前的代码

```java
public Result getXXXX(@Valid @RequestBody GetSignParamResp request){
  // todo
}
```

升级后的代码

```java
public String getXXXX(HttpServletRequest request){
  // todo
}
```

优点：不用动脑子，面向过程开发，适合快速迭代

缺点：耦合性太强，只能满足当前需求，毫无拓展性，且改动较大

## 2、实现HandlerInterceptor接口

重写preHandle方法

```plain
public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
```

这个也是小七以前的同事采取的思路，但是这样写只适合验签这一种需求。
优点：不需要改变原有接口。

缺点：preHandle只能满足不修改原有接口的情况下，满足验签的需求。如果要改变body，还需要实现自己的wapper，实现较为复杂，并且每新增一种加密方式就需要把所有的代码再写一次。

## 3、继承RequestBodyAdviceAdapter以及ResponseBodyAdvice

RequestBodyAdviceAdapter针对加签和解密

ResponseBodyAdvice针对加密

优点：不需要改动代码，可以满足不同平台不同加解密的需求，只需要各自实现以上两个类即可

缺点：只有入参为@RequestBody标记，且返回值为@ResponseBody的方法才能被拦截，不适用于所有场景，比如getXXX()没有入参的方法

## 4、使用注解+AOP

废话少说，直接上代码：

自定义注解

```java
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SecretAnnotation {
    /**
     * 编码
     *
     * @return boolean
     */
    boolean encode() default false;

    /**
     * 解码
     *
     * @return boolean
     */
    boolean decode() default false;

    /**
     * 签名
     * 拓展字段，暂未想到切点
     *
     * @return boolean
     */
    boolean sign() default false;

    /**
     * 验签
     *
     * @return boolean
     */
    boolean verifySign() default false;

    /**
     * 实际调用的方法，必须是SignService的子类
     *
     * @return {@link Class<? extends  SecretService >}
     */
    Class<? extends SecretService> bean() default DefaultSecretService.class;
}
```

基础接口

```java
public interface SecretService {
    /**
     * 加签
     *
     * @param headers 头
     * @param body    身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String> sign(Map<String,String> headers,String body);

    /**
     * 验签
     *
     * @param headers 头
     * @param body    身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String>  verifySign(Map<String,String> headers,String body);

    /**
     * 加密
     *
     * @param httpHeaders http头信息
     * @param body        身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String> encrypt(Map<String,String> httpHeaders, Object body);

    /**
     * 解密
     *
     * @param headers 头
     * @param body    身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String> decrypt(Map<String,String> headers,String body);
}
```

默认的实现方法

```java
@Slf4j
@Service
public class DefaultSecretService implements SecretService {

    @Override
    public Pair<Boolean, String> sign(Map<String,String> headers,String body) {
        log.info("调用了默认的签名方法");
        return Pair.of(Boolean.TRUE, "调用了默认的签名方法");
    }

    @Override
    public Pair<Boolean, String> verifySign(Map<String,String> headers,String body) {
        log.info("调用了默认的验签方法");
        return Pair.of(Boolean.TRUE, "调用了默认的验签方法");
    }

    @Override
    public Pair<Boolean, String> encrypt(Map<String,String> httpHeaders, Object body) {
        log.info("调用了默认的加密方法");
        return Pair.of(Boolean.TRUE, "调用了默认的加密方法");
    }

    @Override
    public Pair<Boolean, String> decrypt(Map<String,String> headers,String body) {
        log.info("调用了默认的解密方法");
        return Pair.of(Boolean.TRUE, "调用了默认的解密方法");
    }
}
```

切面

```java
@Aspect
@Component
@Slf4j
public class SecretAOPAspect {
    @Value("${api.secret.enable:true}")
    private boolean secretEnable;

    // 定义切点,使用了@SecretAnnotation注解的类 或 使用了@SecretAnnotation注解的方法
    @Pointcut("@within(com.hanhua.api.common.secret.SecretAnnotation) || @annotation(com.hanhua.api.common.secret.SecretAnnotation)")
    public void pointcut(){}

    /**
     * 环绕切面
     * 这个通知定义了验签，加解密的基本逻辑
     * @param point 点
     * @return {@link Object}
     */
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point){
        Object result = null;
        // 获取被代理方法参数
        Object[] args = point.getArgs();
        // 获取被代理对象
        Object target = point.getTarget();
        // 获取通知签名
        MethodSignature signature = (MethodSignature)point.getSignature();
 
        try {

            if (!secretEnable){
                return point.proceed(args);
            }

            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

            assert attributes != null;

            // request对象
            HttpServletRequest request = attributes.getRequest();
            // 获取所有的header
            HashMap<String,String> headersMap = new HashMap<>(16);
            Enumeration headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String key = (String) headerNames.nextElement();
                String value = request.getHeader(key);
                headersMap.put(key, value);
            }
            // 获取被代理方法
            Method pointMethod = target.getClass().getMethod(signature.getName(), signature.getParameterTypes());
            // 获取被代理方法上面的注解@SecretAnnotation
            SecretAnnotation secret = pointMethod.getAnnotation(SecretAnnotation.class);
            // 被代理方法上没有，则说明@SecretAnnotation注解在被代理类上
            if(secret==null){
                secret = target.getClass().getAnnotation(SecretAnnotation.class);
            }

            InputStreamReader inputStreamReader = new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8);
            StringBuilder stringBuilder;
            try (BufferedReader br = new BufferedReader(inputStreamReader)) {
                String line;
                stringBuilder = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line);
                }
            }
            String body = stringBuilder.toString();

            // 获取注解上声明的加解密类
            Class<SecretService> bean = (Class<SecretService>) secret.bean();
            // 是否验签
            boolean IsVerifySign = secret.verifySign();
            // 是否解密
            boolean isDecode = secret.decode();
            // 是否加密
            boolean isEncode = secret.encode();

            SecretService signService = SpringUtil.getBean(bean);
            // 需要验签
            if (IsVerifySign) {
                Pair<Boolean, String> verifySign = signService.verifySign(headersMap, body);
                // 验签不通过
                if (!verifySign.getLeft()) {
                    return isEncode ? signService.encrypt(headersMap, verifySign.getRight()).getRight() : verifySign.getRight();
                }
            }
            // 需要解密
            if (StringUtils.isNotBlank(body) && isDecode) {
                Pair<Boolean, String> decrypt = signService.decrypt(headersMap, body);
                // 解密不通过
                if (!decrypt.getLeft()) {
                    return isEncode ? signService.encrypt(headersMap, decrypt.getRight()).getRight() : decrypt.getRight();
                }
                if (args.length>0){
                    args[0] = JSON.parseObject(decrypt.getRight(),args[0].getClass());
                }
            }
            // 执行请求（被代理的返回值必须是实际返回值的父类，或者同一个类）
            result = point.proceed(args);
            // 加密
            return isEncode ? signService.encrypt(headersMap, result).getRight() : result;
        } catch (Throwable throwable) {
            log.error("切面加密解密错误！",throwable);
        }
        return result;
    }
}
```

SpringUtil工具类

```java
@Component
public class SpringUtil implements ApplicationContextAware {

    private static Logger logger = LoggerFactory.getLogger(SpringUtil.class);

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if(SpringUtil.applicationContext == null) {
            SpringUtil.applicationContext = applicationContext;
        }
        logger.info("ApplicationContext配置成功,applicationContext对象：{}",SpringUtil.applicationContext);
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Object getBean(String name) {
        return getApplicationContext().getBean(name);
    }

    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    public static <T> T getBean(String name,Class<T> clazz) {
        return getApplicationContext().getBean(name,clazz);
    }

}
```

如何使用？

直接在类或者方法上增加注解，如果要实现新的加解密规则，只需要自己再去实现一个SecretService接口的类即可。

```plain
@SecretAnnotation(decode=true, encode=true, verifySign=true, bean= DefaultSecretService.class)

```


# 三、总结

小七觉得写代码，要有自己的思考，不能一直做代码的搬运工。就像小七很喜欢的两句名言一样：

（1）勿在浮沙筑高台

（2）面对对象面对君，不负代码不负卿

