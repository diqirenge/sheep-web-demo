package com.sheep.secret;

import com.sheep.secret.DefaultSecretService;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * 例子 [@SecretAnnotation(decode=true, encode=true, verifySign=true, bean= DefaultSecretService.class)]
 *
 * @author lizy
 * @date 2021/08/04
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SecretAnnotation {
    /**
     * 编码
     *
     * @return boolean
     */
    boolean encode() default false;

    /**
     * 解码
     *
     * @return boolean
     */
    boolean decode() default false;

    /**
     * 签名
     * 拓展字段，暂未想到切点
     *
     * @return boolean
     */
    boolean sign() default false;

    /**
     * 验签
     *
     * @return boolean
     */
    boolean verifySign() default false;

    /**
     * 实际调用的方法，必须是SignService的子类
     *
     * @return {@link Class<? extends  SecretService >}
     */
    Class<? extends SecretService> bean() default DefaultSecretService.class;
}

