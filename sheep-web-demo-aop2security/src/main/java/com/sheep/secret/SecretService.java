package com.sheep.secret;

import org.apache.commons.lang3.tuple.Pair;
import java.util.Map;

/**
 * 加解密接口
 *
 * @author lizy
 * @date 2021/08/04
 */
public interface SecretService {
    /**
     * 加签
     *
     * @param headers 头
     * @param body    身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String> sign(Map<String,String> headers,String body);

    /**
     * 验签
     *
     * @param headers 头
     * @param body    身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String>  verifySign(Map<String,String> headers,String body);

    /**
     * 加密
     *
     * @param httpHeaders http头信息
     * @param body        身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String> encrypt(Map<String,String> httpHeaders, Object body);

    /**
     * 解密
     *
     * @param headers 头
     * @param body    身体
     * @return {@link Pair<Boolean, String>}
     */
    Pair<Boolean, String> decrypt(Map<String,String> headers,String body);
}
