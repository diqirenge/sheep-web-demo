package com.sheep.secret;

import com.sheep.secret.dto.TestDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试controller
 *
 * @author lizy4
 * @date 2021/8/7
 */
@RestController
@RequestMapping("/secret")
public class SecretController {
    @SecretAnnotation(decode=true, encode=true, verifySign=true)
    @GetMapping("/getSome")
    public Object getSome(){
        return "getSome方法执行成功！";
    }

    @SecretAnnotation(decode=true, encode=true, verifySign=true)
    @PostMapping("/addSome")
    public Object addSome(@RequestBody TestDto dto){
        return "addSome执行成功："+dto;
    }

    @PostMapping("/getSome")
    public Object secretAnnotation(){
        return "secretAnnotation方法执行成功！";
    }
}
