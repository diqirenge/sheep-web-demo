package com.sheep.secret;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class DefaultSecretService implements SecretService {

    @Override
    public Pair<Boolean, String> sign(Map<String,String> headers,String body) {
        log.info("调用了默认的签名方法");
        return Pair.of(Boolean.TRUE, "调用了默认的签名方法");
    }

    @Override
    public Pair<Boolean, String> verifySign(Map<String,String> headers,String body) {
        log.info("调用了默认的验签方法");
        return Pair.of(Boolean.TRUE, "调用了默认的验签方法");
    }

    @Override
    public Pair<Boolean, String> encrypt(Map<String,String> httpHeaders, Object body) {
        log.info("调用了默认的加密方法");
        return Pair.of(Boolean.TRUE, "调用了默认的加密方法");
    }

    @Override
    public Pair<Boolean, String> decrypt(Map<String,String> headers,String body) {
        log.info("调用了默认的解密方法");
        return Pair.of(Boolean.TRUE, "{\n" +
                "  \"name\": \"lizy\",\n" +
                "  \"value\": \"18\"\n" +
                "}");
    }
}
