package com.sheep.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * hello world控制器.
 *
 * @author lizy
 * @date 2020/12/28
 */
@RestController
public class HelloWorldController {
    
    /**
     * 你好。
     *
     * @return {@link Object}
     */
    @GetMapping("/hello")
    public Object hello() {
        return "Hello World~";
    }

}
