package com.sheep.swagger.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Usage                : 用户dto
 * Project name         :
 * Author               : lizy4
 * Mail                 : lizy4@chinaexpressair.com
 * Date                 : 2020/12/31
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2020/12/31       lizy4             1.0.0             新建
 *

 */
@Data
@ApiModel(value = "用户DTO", description = "User DTO")
public class UserDTO {
    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id", required = true, example = "1")
    private Integer id;
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名", required = true, example = "第七人格")
    private String name;
    /**
     * 工作岗位
     */
    @ApiModelProperty(value = "工作岗位", required = true, example = "开发")
    private String job;

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", job='" + job + '\'' +
                '}';
    }
}
