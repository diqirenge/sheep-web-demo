package com.sheep.swagger.controller;

import com.sheep.swagger.dto.UserDTO;
import com.sheep.swagger.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/user",produces = "text/html;charset=utf-8")
@Api(tags = "用户管理", value = "用户管理")
@Slf4j
public class UserController {

    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "主键查询（DONE）", notes = "备注")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户编号", dataType = "int", paramType = "path", example = "1")})
    public String getUser(@PathVariable Integer id){
        log.info("多个参数用  @ApiImplicitParams，单个参数用 @ApiImplicitParam");
        return "用户id："+id;
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除用户（DONE）", notes = "备注")
    @ApiImplicitParam(name = "id", value = "用户编号", dataType = "int", paramType = "path", example = "1")
    public void delete(@PathVariable Integer id) {
        log.info("删除用户入参-->用户编号：{}",id);
        log.info("单个参数用 ApiImplicitParam");
    }

    @PostMapping("/add")
    @ApiOperation(value = "添加用户（DONE）")
    public UserDTO post(@RequestBody UserDTO user) {
        log.info("新增用户入参-->用户信息：{}",user);
        log.info("如果是 POST PUT 这种带 @RequestBody 的可以不用写 @ApiImplicitParam");
        return user;
    }

    @PutMapping("/modify/{id}")
    @ApiOperation(value = "修改用户（DONE）")
    public void put(@PathVariable Long id, @RequestBody User user) {
        log.info("修改用户入参-->用户信息：{}，用户id：{}",user,id);
        log.info("如果你不想写 @ApiImplicitParam 那么 swagger 也会使用默认的参数名作为描述信息 ");
    }

    @PostMapping("/{id}/file")
    @ApiOperation(value = "文件上传（DONE）")
    public String file(@PathVariable Long id, @RequestParam("file") MultipartFile file) {
        log.info(file.getContentType());
        log.info(file.getName());
        log.info(file.getOriginalFilename());
        return file.getOriginalFilename();
    }
}
