package com.sheep.exception.handler.constant;

/**
 * Usage                : 异常的枚举
 * Project name         :
 * Author               : lizy4
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/01/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/01/12       lizy4             1.0.0             新建
 *
 */
public enum CodeMsg {
    SUCCESS("200", "执行成功"),
    FAILED("500", "操作失败"),

    // 通用的错误码
    SERVER_ERROR("500100", "服务端异常"),
    BIND_ERROR("500101", "参数校验异常：%s"),
    REQUEST_ILLEGAL("500102", "请求非法"),
    ACCESS_LIMIT_REACHED("500104", "访问太频繁！"),
    DATA_NOT_EXIST("500105", "数据不存在！"),
    SIGN_WRONG("500106", "签名错误"),
    SAVE_FILED("500107", "保存失败"),
    REPEAT_BOOK("500108", "重复预定"),
    PARAM_EXP("500109","参数错误"),
    IDENTITY_ERROR("500110","身份信息不一致");

    CodeMsg(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private final String code;

    private final String msg;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
