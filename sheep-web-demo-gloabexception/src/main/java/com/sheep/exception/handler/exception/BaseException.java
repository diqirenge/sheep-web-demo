package com.sheep.exception.handler.exception;

import lombok.EqualsAndHashCode;

/**
 * Usage                : 异常基础类
 * Project name         :
 * Author               : lizy4
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/01/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/01/12       lizy4             1.0.0             新建
 *
 */
@EqualsAndHashCode(callSuper = true)
public class BaseException extends RuntimeException {
	private String code;
	private String subCode;
	private String msg;


	public BaseException() {
	}

	public BaseException(String message, String msg) {
		super(message);
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSubCode() {
		return subCode;
	}

	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
