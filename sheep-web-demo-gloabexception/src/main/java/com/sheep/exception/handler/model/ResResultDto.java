package com.sheep.exception.handler.model;

/**
 * Usage                : 统一返回结果
 * Project name         :
 * Author               : lizy4
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/01/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/01/12       lizy4             1.0.0             新建
 *
 */
public class ResResultDto<T> {
    public static final String STATUS_OK = "OK";
    public static final String STATUS_ERROR = "ERROR";
    public static final String CODE_OK = "200";
    public static final String CODE_ERROR = "-1";
    private String status;
    private String code = "200";
    private String msg;
    private T content;

    public ResResultDto() {
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public String getStatus() {
        return this.status;
    }

    public String getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    public T getContent() {
        return this.content;
    }
}
