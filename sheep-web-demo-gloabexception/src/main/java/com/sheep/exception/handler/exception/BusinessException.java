package com.sheep.exception.handler.exception;

import com.sheep.exception.handler.constant.CodeMsg;

/**
 * Usage                : 业务异常
 * Project name         :
 * Author               : lizy4
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/01/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/01/12       lizy4             1.0.0             新建
 *
 */
public class BusinessException extends BaseException{
    private CodeMsg cm;

    public BusinessException(CodeMsg cm) {
        super.setCode(cm.getCode());
        super.setMsg(cm.getMsg());
        this.cm = cm;
    }

    public CodeMsg getCm() {
        return cm;
    }
}
