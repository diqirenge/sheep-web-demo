package com.sheep.exception.handler.controller;

import com.sheep.exception.handler.constant.CodeMsg;
import com.sheep.exception.handler.exception.BusinessException;
import com.sheep.exception.handler.model.ResResultDto;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Usage                : 测试控制器
 * Project name         :
 * Author               : lizy4
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/01/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/01/12       lizy4             1.0.0             新建
 *
 */
@Slf4j
@RestController
public class TestController {

    @GetMapping(value = "/test/{id}")
    @ApiOperation(value = "主键查询（DONE）", notes = "备注")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户编号", dataType = "int", paramType = "path", example = "1")})
    public ResResultDto<Integer> getUser(@PathVariable Integer id){
        if (id == 1){
            throw new BusinessException(CodeMsg.FAILED);
        }
        if (id == 2){
            throw new BusinessException(CodeMsg.SERVER_ERROR);
        }
        ResResultDto<Integer> resultDto = new ResResultDto<Integer>();
        resultDto.setCode(ResResultDto.CODE_OK);
        resultDto.setStatus(ResResultDto.STATUS_OK);
        resultDto.setContent(id);
        return resultDto;
    }
}
