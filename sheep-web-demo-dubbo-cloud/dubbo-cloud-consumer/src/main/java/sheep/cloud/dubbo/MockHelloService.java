package sheep.cloud.dubbo;

import sheep.cloud.dubbo.api.IHelloService;

public class MockHelloService implements IHelloService {
    @Override
    public String sayHello(String s) {
        return "Sorry，服务无法访问，返回降级数据";
    }
}
