package sheep.cloud.dubbo;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sheep.cloud.dubbo.api.IHelloService;

@RestController
public class HelloController {

    @Reference(mock = "sheep.cloud.dubbo.MockHelloService",
            cluster = "failfast")
    private IHelloService iHelloService;

    @GetMapping("/say")
    public String sayHello(){
        return iHelloService.sayHello("第七人格");
    }
}
