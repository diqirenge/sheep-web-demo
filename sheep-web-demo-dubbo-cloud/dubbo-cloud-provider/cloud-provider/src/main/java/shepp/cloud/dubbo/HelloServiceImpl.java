package shepp.cloud.dubbo;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;
import sheep.cloud.dubbo.api.IHelloService;


/**
 * hello实现类
 *
 * @author lizy
 * @date 2021/08/26
 */
@Service
public class HelloServiceImpl implements IHelloService {
    @Value("${dubbo.application.name}")
    private String serviceName;

    @Override
    public String sayHello(String name) {
        return String.format("[%s]：Hello,%s",serviceName,name);
    }
}
