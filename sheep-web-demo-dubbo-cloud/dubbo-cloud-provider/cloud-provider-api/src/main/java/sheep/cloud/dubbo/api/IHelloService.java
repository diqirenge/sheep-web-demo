package sheep.cloud.dubbo.api;


/**
 * ihello服务
 *
 * @author lizy
 * @date 2021/08/26
 */
public interface IHelloService {
    String sayHello(String name);
}
