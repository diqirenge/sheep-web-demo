package sheep.dubbo.consumer;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sheep.dubbo.api.IHelloService;


/**
 * hello控制器
 *
 * @author lizy
 * @date 2021/08/26
 */
@RestController
public class HelloController {

    @Reference(url = "dubbo://localhost:20880/sheep.dubbo.api.IHelloService")
    private IHelloService helloService;

    @GetMapping("/say")
    public String sayHello(){
        return helloService.sayHello("第七人格");
    }

}
