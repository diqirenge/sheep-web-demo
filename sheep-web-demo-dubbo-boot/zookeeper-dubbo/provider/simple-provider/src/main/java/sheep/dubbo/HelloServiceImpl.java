package sheep.dubbo;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;
import sheep.dubbo.api.DubboTest;
import sheep.dubbo.api.IHelloService;
import sheep.dubbo.api.Request;

import java.util.List;


/**
 * hello实现类
 *
 * @author lizy
 * @date 2021/08/26
 */
@Service
public class HelloServiceImpl implements IHelloService {
    @Value("${dubbo.application.name}")
    private String serviceName;

    @Override
    public String sayHello(String name) {
        return String.format("[%s]：Hello,%s",serviceName,name);
    }

    @Override
    public DubboTest genericInvokeSingleIncludGeneric(Request<DubboTest> dubboTest) {
        return dubboTest.getData();
    }

    @Override
    public DubboTest genericInvokeSingle(DubboTest dubboTest) {
        return dubboTest;
    }

    @Override
    public DubboTest genericInvokeSingleList(List<DubboTest> dubboTest) {
        return dubboTest.get(0);
    }

}
