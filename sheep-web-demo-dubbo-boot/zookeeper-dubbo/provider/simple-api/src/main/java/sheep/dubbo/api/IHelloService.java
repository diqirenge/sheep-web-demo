package sheep.dubbo.api;


import java.util.List;

/**
 * ihello服务
 *
 * @author lizy
 * @date 2021/08/26
 */
public interface IHelloService {
    String sayHello(String name);

    /**
     * 单个参数的泛化调用，包含泛型
     *
     * @param dubboTest 达博测试
     * @return {@link DubboTest}
     */
    DubboTest genericInvokeSingleIncludGeneric(Request<DubboTest> dubboTest);

    /**
     * 单个参数的泛化调用，不包含泛型
     *
     * @param dubboTest 达博测试
     * @return {@link DubboTest}
     */
    DubboTest genericInvokeSingle(DubboTest dubboTest);

    /**
     * 单个参数的泛化调用，list
     *
     * @param dubboTest 达博测试
     * @return {@link DubboTest}
     */
    DubboTest genericInvokeSingleList(List<DubboTest> dubboTest);
}
