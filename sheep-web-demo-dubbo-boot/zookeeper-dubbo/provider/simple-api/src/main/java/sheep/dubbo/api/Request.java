//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package sheep.dubbo.api;

public class Request<T>{
    private static final long serialVersionUID = 1L;
    private T data;

    public Request() {
    }

    public Request(T data) {
        this.data = data;
    }

    public static <T> Request<T> create() {
        Request<T> result = new Request();
        return result;
    }

    public Request<T> data(T data) {
        this.data = data;
        return this;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
