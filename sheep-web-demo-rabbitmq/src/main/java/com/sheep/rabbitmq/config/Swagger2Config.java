package com.sheep.rabbitmq.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Usage                : swagger2配置
 * Project name         :
 * Author               : lizy4
 * Mail                 : lizy4@chinaexpressair.com
 * Date                 : 2020/12/31
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2020/12/31       lizy4             1.0.0             新建
 *

 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sheep.rabbitmq.producer"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("rabbitmq-demo")
                .description("测试SpringBoot整合进行各种工作模式信息的发送")
                .contact(new Contact("李宗洋", "www.52javaee.com", "760470497@qq.com"))
                .version("1.0.0-SNAPSHOT")
                .build();
    }

}