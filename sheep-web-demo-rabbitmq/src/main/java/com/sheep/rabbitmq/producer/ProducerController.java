package com.sheep.rabbitmq.producer;

import cn.hutool.core.date.DateUtil;
import com.sheep.rabbitmq.constants.RabbitConsts;
import com.sheep.rabbitmq.message.MessageStruct;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 生产者控制器
 *
 * @author lizy
 * @date 2021/02/18
 */
@RestController
@Api(tags = "测试SpringBoot整合进行各种工作模式信息的发送", value = "测试SpringBoot整合进行各种工作模式信息的发送")
public class ProducerController {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@ApiOperation(value="测试直接模式发送接口",notes="测试直接模式发送")
	@GetMapping(value="/sendDirect")
	public void sendDirect(String message) {
		rabbitTemplate.convertAndSend(RabbitConsts.DIRECT_MODE_QUEUE_ONE, new MessageStruct("direct message: " + message));
	}

	/**
	 * 测试分列模式发送
	 */
	@ApiOperation(value="测试分列模式发送接口",notes="测试分列模式发送")
	@GetMapping(value="/sendFanout")
	public void sendFanout(String message) {
		rabbitTemplate.convertAndSend(RabbitConsts.FANOUT_MODE_QUEUE, "", new MessageStruct("fanout message: " + message));
	}

	/**
	 * 测试主题模式发送1
	 */
	@ApiOperation(value="测试主题模式发送1接口",notes="测试主题模式发送1")
	@GetMapping(value="/sendTopic1")
	public void sendTopic1(String message) {
		rabbitTemplate.convertAndSend(RabbitConsts.TOPIC_MODE_QUEUE, "queue.aaa.bbb", new MessageStruct("topic message: " + message));
	}

	/**
	 * 测试主题模式发送2
	 */
	@ApiOperation(value="测试主题模式发送2接口",notes="测试主题模式发送2")
	@GetMapping(value="/sendTopic2")
	public void sendTopic2(String message) {
		rabbitTemplate.convertAndSend(RabbitConsts.TOPIC_MODE_QUEUE, "ccc.queue", new MessageStruct("topic message: " + message));
	}

	/**
	 * 测试主题模式发送3
	 */
	@ApiOperation(value="测试主题模式发送3接口",notes="测试主题模式发送3")
	@GetMapping(value="/sendTopic3")
	public void sendTopic3(String message) {
		rabbitTemplate.convertAndSend(RabbitConsts.TOPIC_MODE_QUEUE, "3.queue", new MessageStruct("topic message: " + message));
	}

	/**
	 * 测试延迟队列发送
	 */
	@ApiOperation(value="测试延迟队列发送接口",notes="测试延迟队列发送")
	@GetMapping(value="/sendDelay")
	public void sendDelay() {
		rabbitTemplate.convertAndSend(RabbitConsts.DELAY_MODE_QUEUE, RabbitConsts.DELAY_QUEUE, new MessageStruct("delay message, delay 5s, " + DateUtil
				.date()), message -> {
			message.getMessageProperties().setHeader("x-delay", 5000);
			return message;
		});
		rabbitTemplate.convertAndSend(RabbitConsts.DELAY_MODE_QUEUE, RabbitConsts.DELAY_QUEUE, new MessageStruct("delay message,  delay 2s, " + DateUtil
				.date()), message -> {
			message.getMessageProperties().setHeader("x-delay", 2000);
			return message;
		});
		rabbitTemplate.convertAndSend(RabbitConsts.DELAY_MODE_QUEUE, RabbitConsts.DELAY_QUEUE, new MessageStruct("delay message,  delay 8s, " + DateUtil
				.date()), message -> {
			message.getMessageProperties().setHeader("x-delay", 8000);
			return message;
		});
	}

	/**
	 * 下单完成会发送消息到延迟队列 用于自动取消订单
	 */
	@ApiOperation(value="测试延迟队列发送接口（订单取消）",notes="测试延迟队列发送（订单取消）")
	@GetMapping(value="/synSendCancelOrderMessage")
	public void synSendCancelOrderMessage(){
		rabbitTemplate.convertAndSend(RabbitConsts.X_DEAD_LETTER_EXCHANGE,
				RabbitConsts.CANCELORDER_SEND_ROUTINGKEY, new MessageStruct("自动取消订单"), message -> {
					message.getMessageProperties().setExpiration(String.valueOf(10 * 1000));
					return message;
				});
	}

}
