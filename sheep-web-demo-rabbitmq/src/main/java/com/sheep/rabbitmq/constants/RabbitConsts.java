package com.sheep.rabbitmq.constants;


/**
 * RabbitMQ常量池
 *
 * @author lizy
 * @date 2021/02/19
 */
public interface RabbitConsts {
    /**
     * 直接模式1
     */
    String DIRECT_MODE_QUEUE_ONE = "queue.direct.1";

    /**
     * 队列2
     */
    String QUEUE_TWO = "queue.2";

    /**
     * 队列3
     */
    String QUEUE_THREE = "3.queue";

    /**
     * 分列模式
     */
    String FANOUT_MODE_QUEUE = "fanout.mode";

    /**
     * 主题模式
     */
    String TOPIC_MODE_QUEUE = "topic.mode";

    /**
     * 路由1
     */
    String TOPIC_ROUTING_KEY_ONE = "queue.#";

    /**
     * 路由2
     */
    String TOPIC_ROUTING_KEY_TWO = "*.queue";

    /**
     * 路由3
     */
    String TOPIC_ROUTING_KEY_THREE = "3.queue";

    /**
     * 延迟队列
     */
    String DELAY_QUEUE = "delay.queue";

    /**
     * 延迟队列交换器
     */
    String DELAY_MODE_QUEUE = "delay.mode";

    //=====================TTL+死信队列实现======================//
    /**
     * 声明了队列里的死信转发到的DLX名称
     */
    String X_DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";
    /**
     * 声明了这些死信在转发时携带的 routing-key 名称
     */
    String X_DEAD_LETTER_ROUTING_KEY = "x-dead-letter-routing-key";
    /**
     * 消息过期后会转到该队列中，需要消费者监听并消费
     */
    String CANCELORDER_CONSUME_QUEUE = "cancelOrderConsumeQueue";
    /**
     * 声明了生产者在发送消息时携带的 routing-key 名称
     */
    String CANCELORDER_SEND_ROUTINGKEY = "cancelOrderSendRoutingKey";
    /**
     * 声明了生产者在发送消息时携带的 Queue 名称
     */
    String CANCELORDER_QUEUE = "cancelOrderSendQueue";

}
