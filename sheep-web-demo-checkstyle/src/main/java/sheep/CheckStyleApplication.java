package sheep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 应用程序.
 *
 * @author lizongyang
 * @date 2023/04/17
 */
@SpringBootApplication
public class CheckStyleApplication {

    /**
     * 主要
     *
     * @param args arg游戏
     */
    public static void main(String[] args) {
        SpringApplication.run(CheckStyleApplication.class, args);
    }

}
