# sheep-web-demo-checkstyle

## checkstyle maven插件的使用

执行以下命令
> mvn validate -pl sheep-web-demo-checkstyle -am


出现错误
`\sheep-web-demo\sheep-web-demo-checkstyle\src\main\java\sheep\CheckStyleApplication.java:15: Javadoc的第一句缺少一个结束时期。 [SummaryJavadoc]`

“主要”后面加入英文句号，再次执行
> mvn validate -pl sheep-web-demo-checkstyle -am

校验通过