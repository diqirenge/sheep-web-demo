package com.sheep.loader;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URL;
import java.util.Optional;

public final class JarPathBuilder {

    /**
     * 默认ext插件路径
     * 可以暴露出去，做到参数控制
     */
    private static final String DEFAULT_EXT_PLUGIN_PATH = "/ext-lib/";

    /**
     * 得到jar路径
     *
     * @param path 路径
     * @return {@link File}
     */
    public static File getJarPath(final String path) {
        if (StringUtils.isNotEmpty(path)) {
            System.out.println("开始加载【" + path + "】路径下的jar包");
            return new File(path);
        }
        System.out.println("开始加载【ext-lib】路径下的jar包");
        return buildJarPath();
    }

    /**
     * 构建jar路径
     *
     * @return {@link File}
     */
    private static File buildJarPath() {
        URL url = JarPathBuilder.class.getResource(DEFAULT_EXT_PLUGIN_PATH);
        return Optional.ofNullable(url).map(u -> new File(u.getFile())).orElse(new File(DEFAULT_EXT_PLUGIN_PATH));
    }

}
