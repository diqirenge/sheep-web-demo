package com.sheep.convertor.dto;

import com.sheep.convertor.model.Specialty;
import lombok.Data;

import java.util.Date;
import java.util.List;


/**
 * Usage                : 男人DTO。
 * Project name         :
 * Author               : 第七人格
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/03/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/03/12       第七人格             1.0.0             新建
 *
 */
@Data
public class ManDto {
    private String nameDto;
    private Integer ageDto;
    private List<Specialty> specialtyDto;
    private Date birthdayDto;
}
