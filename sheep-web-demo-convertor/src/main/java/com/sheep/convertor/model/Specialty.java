package com.sheep.convertor.model;

import lombok.Data;

/**
 * Usage                : 特长。
 * Project name         :
 * Author               : 第七人格
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/03/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/03/12       第七人格             1.0.0             新建
 *
 */
@Data
public class Specialty {
    private String name;
}
