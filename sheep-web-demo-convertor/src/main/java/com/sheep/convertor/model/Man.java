package com.sheep.convertor.model;

import com.sheep.convertor.base.BaseModelMapper;
import com.sheep.convertor.dto.ManDto;
import lombok.Data;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Date;
import java.util.List;


/**
 * Usage                : 男人。
 * Project name         :
 * Author               : 第七人格
 * Mail                 : 760470497@qq.com
 * Date                 : 2021/03/12
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2021/03/12       第七人格             1.0.0             新建
 *
 */
@Data
public class Man {
    private String name;
    private Integer age;
    private List<Specialty> specialty;
    private Date birthday;


    @Mapper
    public interface ManModelMapper extends BaseModelMapper<ManDto, Man> {
        /**
         * 转换Dto对象到实体对象。
         *
         * @param exerciseStatsDto 域对象。
         * @return 实体对象。
         */
        @Override
        @Mapping(source = "nameDto", target = "name")
        @Mapping(source = "ageDto", target = "age")
        @Mapping(source = "specialtyDto", target = "specialty")
        @Mapping(source = "birthdayDto", target = "birthday",dateFormat = "yyyy-MM-dd HH:mm:ss")
        Man toModel(ManDto exerciseStatsDto);
        /**
         * 转换实体对象到Dto对象。
         *
         * @param exerciseStats 实体对象。
         * @return 域对象。
         */
        @Override
        @Mapping(source = "name", target = "nameDto")
        @Mapping(source = "age", target = "ageDto")
        @Mapping(source = "specialty", target = "specialtyDto")
        @Mapping(source = "birthday", target = "birthdayDto",dateFormat = "yyyy-MM-dd HH:mm:ss")
        ManDto fromModel(Man exerciseStats);
    }
    public static final ManModelMapper INSTANCE = Mappers.getMapper(ManModelMapper.class);
}
