package com.sheep.convertor.model;


import cn.hutool.core.date.DateUtil;
import com.sheep.convertor.dto.ManDto;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lizy4
 * @className ManTest
 * @date 2021/3/12 9:50
 * @description
 */
public class ManTest {
    @Test
    public void Test1(){
        // 测试对象转换
        Man man = new Man();
        man.setAge(10);
        man.setName("小明");
        man.setBirthday(DateUtil.date());
        Specialty specialty = new Specialty();
        specialty.setName("打篮球");
        Specialty specialty1 = new Specialty();
        specialty1.setName("写作");
        List<Specialty> specialtyList = new ArrayList<>(16);
        specialtyList.add(specialty);
        specialtyList.add(specialty1);
        man.setSpecialty(specialtyList);

        ManDto manDto = Man.INSTANCE.fromModel(man);
        System.out.println("manDto："+manDto);

        Man man1 = Man.INSTANCE.toModel(manDto);
        System.out.println("man："+man1);

        // 测试list转换
        Man man2 = new Man();
        man2.setAge(10);
        man2.setName("小东");
        man2.setBirthday(DateUtil.date());
        Specialty specialty2 = new Specialty();
        specialty2.setName("踢足球");
        Specialty specialty12 = new Specialty();
        specialty12.setName("玩游戏");
        List<Specialty> specialtyList2 = new ArrayList<>(16);
        specialtyList2.add(specialty2);
        specialtyList2.add(specialty12);

        man2.setSpecialty(specialtyList2);

        List<Man> manlist = new ArrayList<>(16);
        manlist.add(man2);
        manlist.add(man);

        List<ManDto> manDtoList = Man.INSTANCE.fromModelList(manlist);
        System.out.println("manDtoList："+manDtoList);

        List<Man> manList = Man.INSTANCE.toModelList(manDtoList);
        System.out.println("manList："+manList);
    }

}