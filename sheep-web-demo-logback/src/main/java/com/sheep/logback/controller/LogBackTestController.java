package com.sheep.logback.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Usage                : 日志重新测试控制器
 * Project name         :
 * Author               : lizy4
 * Mail                 : lizy4@chinaexpressair.com
 * Date                 : 2020/12/29
 * Version              : 1.0.0
 * Modification history :
 * Date          Author          Version          Description
 * ---------------------------------------------------------------
 * 2020/12/29       lizy4             1.0.0             新建
 *

 */
@RestController
@RequestMapping("/http")
@Slf4j
public class LogBackTestController {
    @GetMapping("/soutLog/{param}")
    public String soutLog(@PathVariable(value = "param") String param){
        log.info("入参：{}",param);
        return param;
    }

}
