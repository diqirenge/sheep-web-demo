package com.sheep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogoApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(LogoApplication.class);
        //关闭启动 logo 的输出
//        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);
    }

}
