package sheep.retry.controller;
import java.math.BigDecimal;

import cn.hutool.json.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sheep.retry.PayService;
import sheep.retry.ZhulongHDPushApplyResult;

/**
 * hello world控制器
 *
 * @author lizy
 * @date 2020/12/28
 */
@RestController
@Slf4j
public class PayController {
    @Autowired
    private PayService payService;

    /**
     * 你好，gay
     *
     * @return {@link Object}
     */
    @GetMapping("/pay")
    public Object hello(String a) throws Exception {
        int i = payService.minGoodsnum(a);
        log.info("库存为：【{}】", i);
        return "库存为：【"+i+"】";
    }

    @GetMapping("/hi")
    public Object hi(Object a) throws Exception {
        ZhulongHDPushApplyResult zhulongHDPushApplyResult = new ZhulongHDPushApplyResult();
        zhulongHDPushApplyResult.setOrderId("nb");
        zhulongHDPushApplyResult.setOrderStatus("nb");
        zhulongHDPushApplyResult.setGuaranteeId("nb");
        zhulongHDPushApplyResult.setValidateCode("nb");
        zhulongHDPushApplyResult.setInsuranceCompany("nb");
        zhulongHDPushApplyResult.setPayStatus(0);
        zhulongHDPushApplyResult.setBaoFei(new BigDecimal("0"));
        zhulongHDPushApplyResult.setJumpUrl("nb");
        zhulongHDPushApplyResult.setGuaranteeUrl("nb");
        zhulongHDPushApplyResult.setTimeStamp("nb");
        zhulongHDPushApplyResult.setPlatformCode("nb");
        zhulongHDPushApplyResult.setApplyResultUrl("nb");
        zhulongHDPushApplyResult.setSignStr("nb");

        return "hi："+ payService.hi(zhulongHDPushApplyResult).toString();
    }
}
