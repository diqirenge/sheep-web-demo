package sheep.retry;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

/**
 * @author lizy4
 * @className PayService
 * @date 2021/2/5 23:09
 * @description
 */
@Service
@Slf4j
public class PayService {
    private final int totalNum = 100000;

    @Retryable(recover = "minGoodsnumRecover",value = Exception.class, maxAttempts = 3, backoff = @Backoff(delay = 2000L, multiplier = 1.5))
    public int minGoodsnum(String num) throws Exception {
        int numS = Integer.valueOf(num);
        log.info("减库存开始【{}】" , DateUtil.now());
        if (numS <= 0) {
            throw new Exception("数量不对");
        }
        log.info("减库存执行结束【{}】" , DateUtil.now());
        return totalNum - numS;
    }

    @Recover
    public int minGoodsnumRecover(Throwable throwable,String num) {
        log.info("执行了兜底方案！！！",throwable);
        return totalNum;
    }

    @Retryable(recover = "compensateHi",value = Exception.class, maxAttempts = 3, backoff = @Backoff(delay = 2000L, multiplier = 1.5))
    public ZhulongHDPushApplyResult hi(ZhulongHDPushApplyResult name) throws Exception {
        try {
            log.info("1111111111111"+name);
            if (1==1 ){
                throw new Exception("222222");
            }
        }catch (Exception e){
            throw new Exception("222222");
        }
        return name;
    }

    @Recover
    //Throwable throwable必须写,否则无法匹配
    private ZhulongHDPushApplyResult compensateHi(Throwable throwable,ZhulongHDPushApplyResult name) throws Exception {
        System.out.println("compensateHi");
        try {
            log.info("执行了兜底方案");
        }catch (Exception e){
            throw new Exception("222222");
        }
        return name;
    }




}
