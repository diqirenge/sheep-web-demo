package sheep.retry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
// 开启重试支持
@EnableRetry
public class SpringRetryApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(SpringRetryApplication.class);
        springApplication.run(args);
    }

}
